const config = require('../knexfile.js');
const db = require('knex')(config);

module.exports = {
  getConnection: () => db,
  destroyConnection: () => db.destroy(),
};
