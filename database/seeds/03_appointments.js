const moment = require('moment');
const sinon = require('sinon');
const { v4: uuidv4 } = require('uuid');
const { APPOINTMENT_STATES } = require('../../lib/utils/constants.js');
const QrGeneratorService = require('../../lib/services/qr_generator_service.js');
const oneWeekAgo = moment().subtract(7, 'days');
const twoWeeksAgo = moment().subtract(14, 'days');
const threeWeeksAgo = moment().subtract(21, 'days');

const mockLogger = {
  debug: sinon.stub(),
  error: sinon.stub(),
  warn: sinon.stub(),
  info: sinon.stub(),
};
const qrGeneratorService = new QrGeneratorService(mockLogger);

exports.seed = function(knex) {
  // Deletes ALL existing entries
  return Promise.all([
    knex('appointment').del()
        .then(async function() {
        // Inserts seed entries
          const appointments = await Promise.all([
            getAppointment(1, '123456','2','5','Fiebre alta durante 4 dias', APPOINTMENT_STATES.FINISHED, oneWeekAgo ),
            getAppointment(2, '1111','1','5','Dolor de estomago', APPOINTMENT_STATES.FINISHED, twoWeeksAgo ),
            getAppointment(3, '4898','2','5','Insomnio frecuente', APPOINTMENT_STATES.FINISHED, threeWeeksAgo ),
            getAppointment(4, '4898','1','16','Creo que tengo tunel carpiano', APPOINTMENT_STATES.FINISHED, oneWeekAgo ),
            getAppointment(5, '123456','1','23','Me duele la vista', APPOINTMENT_STATES.FINISHED, twoWeeksAgo ),
            getAppointment(6, '1111','2','3','Quiero hacerme un chequeo', APPOINTMENT_STATES.FINISHED, threeWeeksAgo ),
            getAppointment(7, '101010', undefined,'22','Me duele una muela', APPOINTMENT_STATES.QUEUED),
            getAppointment(8, '2222', undefined,'5','Con esta cuarentena, me van a enfermar', APPOINTMENT_STATES.QUEUED),
            getAppointment(9, '3333', undefined,'16','Senti un pinchazo en el ultimo partido', APPOINTMENT_STATES.QUEUED),
            getAppointment(10, '4444', undefined,'5','Usá ConectarSalud y quedate en casa', APPOINTMENT_STATES.QUEUED),
            getAppointment(11, '5555', undefined,'5','Con ConectarSalud, no se sale más!', APPOINTMENT_STATES.QUEUED),
            getAppointment(12, '6666', undefined,'16','Me levanté con dolor del ciático', APPOINTMENT_STATES.QUEUED),
          ]);
          return knex('appointment').insert(appointments);
        })
    // Add the following to avoid issues with upcoming inserts
        .then(function() {
          return knex.raw('select setval(\'appointment_appointment_id_seq\', max(appointment_id)) from appointment');
        }),
    knex('appointment_rating').del(),
  ]);
};

async function getAppointment(id, memberId, doctorId, medicalSpecialtyId, reason, status, createdAt){
  const channelId = uuidv4();
  const appointment = {
    appointment_id: id,
    member_id: memberId,
    medical_specialty_id: medicalSpecialtyId,
    reason: reason,
    status: status,
    channel_id: channelId,
  };
  if (doctorId){
    appointment.doctor_id = doctorId;
  }
  if (createdAt){
    appointment.created_at = createdAt;
  }
  const qrContent = {
    appointment_id: appointment.appointment_id,
    channel_id: appointment.channel_id,
  };
  let qrResource;
  try {
    qrResource = await qrGeneratorService.generateQrAndUpload(qrContent);
  } catch (err) {
    throw err;
  }
  appointment.connection_qr = qrResource;
  return appointment;
}
