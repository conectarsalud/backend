const crypto = require('crypto');

const hoursMondayToFriday = JSON.stringify([{ day: '1', start_time: '8', end_time: '16' },
  { day: '2', start_time: '8', end_time: '16' },
  { day: '3', start_time: '8', end_time: '16' },
  { day: '4', start_time: '8', end_time: '16' },
  { day: '5', start_time: '8', end_time: '16' }]);

const hoursMondayThursday = JSON.stringify([{ day: '1', start_time: '8', end_time: '10' },
  { day: '1', start_time: '16', end_time: '18' },
  { day: '4', start_time: '8', end_time: '10' },
  { day: '4', start_time: '16', end_time: '18' }]);

const hoursMonday = JSON.stringify([{ day: '1', start_time: '17', end_time: '22' }]);

const hoursTuesdaySaturday = JSON.stringify([{ day: '2', start_time: '10', end_time: '13' },
  { day: '5', start_time: '10', end_time: '13' }]);

exports.seed = function(knex) {
  // Deletes ALL existing entries
  return Promise.all([
    knex('medical_specialty').del()
        .then(function() {
        // Inserts seed entries
          return knex('medical_specialty').insert([
            { medical_specialty_id: '1', name: 'Alergia e Inmunología' },
            { medical_specialty_id: '2', name: 'Andrología' },
            { medical_specialty_id: '3', name: 'Cardiología' },
            { medical_specialty_id: '4', name: 'Cirugía' },
            { medical_specialty_id: '5', name: 'Clínica médica' },
            { medical_specialty_id: '6', name: 'Dermatología' },
            { medical_specialty_id: '7', name: 'Ecografía' },
            { medical_specialty_id: '8', name: 'Electrocardiografía' },
            { medical_specialty_id: '9', name: 'Endocrinología' },
            { medical_specialty_id: '10', name: 'Fonoaudiología' },
            { medical_specialty_id: '11', name: 'Gastroenterología' },
            { medical_specialty_id: '12', name: 'Geriatría' },
            { medical_specialty_id: '13', name: 'Ginecología' },
            { medical_specialty_id: '14', name: 'Hematología' },
            { medical_specialty_id: '15', name: 'Infectología' },
            { medical_specialty_id: '16', name: 'Kinesiología' },
            { medical_specialty_id: '17', name: 'Laboratorio' },
            { medical_specialty_id: '18', name: 'Neumonología' },
            { medical_specialty_id: '19', name: 'Neurología' },
            { medical_specialty_id: '20', name: 'Nutrición' },
            { medical_specialty_id: '21', name: 'Obstetricia' },
            { medical_specialty_id: '22', name: 'Odontología' },
            { medical_specialty_id: '23', name: 'Oftalmología' },
            { medical_specialty_id: '24', name: 'Oncología' },
            { medical_specialty_id: '25', name: 'Otorrinolaringología' },
            { medical_specialty_id: '26', name: 'Pediatría' },
            { medical_specialty_id: '27', name: 'Psicología' },
            { medical_specialty_id: '28', name: 'Psiquiatría' },
            { medical_specialty_id: '29', name: 'Radiología' },
            { medical_specialty_id: '30', name: 'Traumatología' },
            { medical_specialty_id: '31', name: 'Urología' },
          ]);
        }),
    knex('doctor').del()
        .then(function() {
        // Inserts seed entries
          return knex('doctor').insert([
            getDoctor('1', 'Gines González García', 'Cirugía', hoursMondayToFriday, '1111'),
            getDoctor('2', 'René Favaloro', 'Cardiología', hoursMondayThursday, '2222'),
            getDoctor('3', 'Alberto Cormillot', 'Nutrición', hoursMonday, '3333'),
            getDoctor('4', 'Daniel López Rosetti', 'Cardiología, Clínica médica, Psicología', hoursTuesdaySaturday, '4444'),
            getDoctor('5', 'Guillermo Capuya', 'Cirugía, Urología', hoursMondayToFriday),
            getDoctor('6', 'Gabriel Rolón', 'Psicología', hoursTuesdaySaturday),
            getDoctor('7', 'Pedro Cahn', 'Infectología', hoursMondayThursday),
            getDoctor('8', 'Diego Martínez', 'Clínica médica', hoursMonday),
            getDoctor('9', 'Facundo Manes', 'Neurología', hoursTuesdaySaturday),
            getDoctor('10', 'Jorge Tartaglione', 'Cardiología', hoursMondayToFriday),
            getDoctor('11', 'Bernardo Alberto Houssey', 'Endocrinología', hoursTuesdaySaturday),
            getDoctor('12', 'Cecilia Grierson', 'Clínica médica', hoursMondayThursday),
            getDoctor('13', 'Luis Agote', 'Clínica médica', hoursMonday),
            getDoctor('14', 'Romina Pereiro', 'Nutrición', hoursTuesdaySaturday),
            getDoctor('15', 'Ramón Carrillo', 'Cirugía, Neurología', hoursMondayToFriday),
            getDoctor('16', 'Arturo Humberto Illia', 'Clínica médica', hoursTuesdaySaturday),
            getDoctor('17', 'Adrián Cormillot', 'Nutrición', hoursMondayThursday),
            getDoctor('18', 'Luis Braverman', 'Odontología', hoursMonday),
            getDoctor('19', 'Salvador Mazza', 'Clínica médica', hoursTuesdaySaturday),
            getDoctor('20', 'José Penna', 'Clínica médica', hoursMondayToFriday),
            getDoctor('21', 'Mariano Rafael Castex', 'Clínica médica', hoursTuesdaySaturday),
            getDoctor('22', 'Carlos Gregorio Malbrán', 'Clínica médica', hoursMondayThursday),
            getDoctor('23', 'Leopoldo Montes de Oca', 'Clínica médica', hoursMonday),
            getDoctor('24', 'Mario Socolinsky', 'Pediatría', hoursTuesdaySaturday),
            getDoctor('25', 'Luis Federico Leloir', 'Clínica médica', hoursMondayToFriday),
            getDoctor('26', 'Enrique Finochietto', 'Cirugía', hoursTuesdaySaturday),
            getDoctor('27', 'Ricardo Finochietto', 'Cirugía', hoursMondayThursday),
            getDoctor('28', 'Alejandro Korn', 'Clínica médica', hoursMonday),
            getDoctor('29', 'Nelson Castro', 'Neurología', hoursTuesdaySaturday),
            getDoctor('30', 'Reinaldo Chacón', 'Oncología', hoursMondayToFriday),
            getDoctor('31', 'Roberto Aquiles Estévez', 'Oncología', hoursTuesdaySaturday),
            getDoctor('32', 'José Arce', 'Cirugía', hoursMondayThursday),
            getDoctor('33', 'Abel Ayerza', 'Cardiología', hoursMonday),
            getDoctor('34', 'Roberto Wernicke', 'Clínica médica', hoursTuesdaySaturday),
            getDoctor('35', 'Pedro Escudero', 'Clínica médica', hoursMondayToFriday),
            getDoctor('36', 'José Ingenieros', 'Clínica médica', hoursTuesdaySaturday),
            getDoctor('37', 'Jorge Jozami', 'Ginecología', hoursMondayThursday),
            getDoctor('38', 'Julio Méndez', 'Infectología', hoursMonday),
            getDoctor('39', 'Pedro Lagleyze', 'Oftalmología', hoursTuesdaySaturday),
            getDoctor('40', 'Enrique del Arca', 'Clínica médica', hoursMondayToFriday),
            getDoctor('41', 'Francisco Sicardi', 'Clínica médica', hoursTuesdaySaturday),
            getDoctor('42', 'Pedro Chutro', 'Cirugía', hoursMondayThursday),
            getDoctor('43', 'Armando Basso', 'Cirugía, Neurología', hoursMonday),
            getDoctor('44', 'Adolfo Rubinstein', 'Clínica médica', hoursTuesdaySaturday),
            getDoctor('45', 'Héctor Lombardo', 'Clínica médica', hoursMondayToFriday),
            getDoctor('46', 'José Luis Manzur', 'Cirugía', hoursTuesdaySaturday),
            getDoctor('47', 'Eduardo Tassano', 'Cardiología', hoursMondayThursday),
            getDoctor('48', 'Oscar Herrera Ahuad', 'Pediatría', hoursMonday),
            getDoctor('49', 'Pablo Raúl Yedlin', 'Pediatría', hoursTuesdaySaturday),
            getDoctor('50', 'José Manuel Cano', 'Odontología', hoursMondayToFriday),
          ]);
        }),
    knex('doctor_medical_specialty').del()
        .then(function() {
        // Inserts seed entries
          return knex('doctor_medical_specialty').insert([
            { doctor_id: '1', medical_specialty_id: '4' },
            { doctor_id: '2', medical_specialty_id: '3' },
            { doctor_id: '3', medical_specialty_id: '20' },
            { doctor_id: '4', medical_specialty_id: '5' },
            { doctor_id: '4', medical_specialty_id: '3' },
            { doctor_id: '4', medical_specialty_id: '27' },
            { doctor_id: '5', medical_specialty_id: '4' },
            { doctor_id: '5', medical_specialty_id: '31' },
            { doctor_id: '6', medical_specialty_id: '27' },
            { doctor_id: '7', medical_specialty_id: '15' },
            { doctor_id: '8', medical_specialty_id: '5' },
            { doctor_id: '9', medical_specialty_id: '19' },
            { doctor_id: '10', medical_specialty_id: '3' },
            { doctor_id: '11', medical_specialty_id: '9' },
            { doctor_id: '12', medical_specialty_id: '5' },
            { doctor_id: '13', medical_specialty_id: '5' },
            { doctor_id: '14', medical_specialty_id: '20' },
            { doctor_id: '15', medical_specialty_id: '19' },
            { doctor_id: '15', medical_specialty_id: '4' },
            { doctor_id: '16', medical_specialty_id: '5' },
            { doctor_id: '17', medical_specialty_id: '20' },
            { doctor_id: '18', medical_specialty_id: '22' },
            { doctor_id: '19', medical_specialty_id: '5' },
            { doctor_id: '20', medical_specialty_id: '5' },
            { doctor_id: '21', medical_specialty_id: '5' },
            { doctor_id: '22', medical_specialty_id: '5' },
            { doctor_id: '23', medical_specialty_id: '5' },
            { doctor_id: '24', medical_specialty_id: '26' },
            { doctor_id: '25', medical_specialty_id: '5' },
            { doctor_id: '26', medical_specialty_id: '4' },
            { doctor_id: '27', medical_specialty_id: '4' },
            { doctor_id: '28', medical_specialty_id: '5' },
            { doctor_id: '29', medical_specialty_id: '19' },
            { doctor_id: '30', medical_specialty_id: '24' },
            { doctor_id: '31', medical_specialty_id: '24' },
            { doctor_id: '32', medical_specialty_id: '4' },
            { doctor_id: '33', medical_specialty_id: '3' },
            { doctor_id: '34', medical_specialty_id: '5' },
            { doctor_id: '35', medical_specialty_id: '5' },
            { doctor_id: '36', medical_specialty_id: '5' },
            { doctor_id: '37', medical_specialty_id: '13' },
            { doctor_id: '38', medical_specialty_id: '15' },
            { doctor_id: '39', medical_specialty_id: '23' },
            { doctor_id: '40', medical_specialty_id: '5' },
            { doctor_id: '41', medical_specialty_id: '5' },
            { doctor_id: '42', medical_specialty_id: '4' },
            { doctor_id: '43', medical_specialty_id: '4' },
            { doctor_id: '43', medical_specialty_id: '19' },
            { doctor_id: '44', medical_specialty_id: '5' },
            { doctor_id: '45', medical_specialty_id: '5' },
            { doctor_id: '46', medical_specialty_id: '4' },
            { doctor_id: '47', medical_specialty_id: '3' },
            { doctor_id: '48', medical_specialty_id: '26' },
            { doctor_id: '49', medical_specialty_id: '26' },
            { doctor_id: '50', medical_specialty_id: '22' },
          ]);
        }),
  ]);
};

function getDoctor(doctorId, name, specialtiesCsv, businessHours, hardcodedMedicalRegistration) {
  const doctor = { doctor_id: doctorId, name: name,
    specialties_csv: specialtiesCsv, business_hours: businessHours };
  doctor.medical_registration_type = 'MN';
  const medicalRegistration = hardcodedMedicalRegistration? hardcodedMedicalRegistration : getMedicalRegistration();
  doctor.medical_registration = medicalRegistration;
  doctor.username = medicalRegistration;
  doctor.password = hash(medicalRegistration);
  return doctor;
}

function getMedicalRegistration() {
  const maxValue = 999999 - 100000; // moving the range to 100000-999999 to ensure it has 6 digits
  return (Math.floor(Math.random() * Math.floor(maxValue)) + 100000).toString();
}

function hash(s) {
  return crypto.createHmac('sha256', 'secret').update(s).digest('hex');
}
