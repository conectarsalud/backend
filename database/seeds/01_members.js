const crypto = require('crypto');

exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('member').del()
      .then(function() {
      // Inserts seed entries
        return knex('member').insert([
          { member_id: '4898', username: '4898', name: 'Gonzalo Casey', password: hash('32237575'), dni: '32237575' },
          { member_id: '1111', username: '1111', name: 'Tomas Lubertino', password: hash('37243062'), dni: '37243062' },
          { member_id: '123456', username: '123456', name: 'Mauro Toscano', password: hash('123456'), dni: '123456' },
          { member_id: '101010', username: '101010', name: 'Diego Maradona', password: hash('101010'), dni: '101010' },
          { member_id: '2222', username: '2222', name: 'Guillermo Francella', password: hash('2222'), dni: '2222' },
          { member_id: '3333', username: '3333', name: 'Lionel Messi', password: hash('3333'), dni: '3333' },
          { member_id: '4444', username: '4444', name: 'Alberto Fernández', password: hash('4444'), dni: '4444' },
          { member_id: '5555', username: '5555', name: 'Mauricio Macri', password: hash('5555'), dni: '5555' },
          { member_id: '6666', username: '6666', name: 'Ricardo Darín', password: hash('6666'), dni: '6666' },
        ]);
      });
};

function hash(s) {
  return crypto.createHmac('sha256', 'secret').update(s).digest('hex');
}
