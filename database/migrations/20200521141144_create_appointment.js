const { onUpdateTrigger } = require('../../knexfile.js');
const { APPOINTMENT_STATES } = require('../../lib/utils/constants.js');

exports.up = function(knex) {
  return Promise.all([
    knex.schema
        .createTable('appointment', function(table) {
          table.increments('appointment_id').unique().primary();
          table.integer('doctor_id').notNullable().references('doctor');
          table.integer('member_id').notNullable().references('member');
          table.string('channel_id', 100);
          table.enum('status', [APPOINTMENT_STATES.QUEUED, APPOINTMENT_STATES.ASSIGNED, APPOINTMENT_STATES.IN_PROGRESS,
            APPOINTMENT_STATES.WAITING_DOCTOR, APPOINTMENT_STATES.WAITING_MEMBER, APPOINTMENT_STATES.FINISHED, APPOINTMENT_STATES.MEMBER_DISCONNECT,
            APPOINTMENT_STATES.DOCTOR_DISCONNECT]).notNullable();
          table.string('connection_qr', 100);
          table.timestamp('member_last_connection_time');
          table.timestamp('doctor_last_connection_time');
          table.timestamp('conference_start_time');
          table.timestamp('conference_end_time');
          table.timestamps(true, true);
        }).then(() =>
          knex.raw(onUpdateTrigger('appointment')),
        ),
    knex.schema
        .createTable('appointment_rating', function(table) {
          table.integer('appointment_id').unique().primary().references('appointment');
          table.integer('rating').notNullable(),
          table.string('comments', 300);
          table.timestamps(true, true);
        }).then(() =>
          knex.raw('alter table appointment_rating add constraint correct_rating check (rating >= 1 and rating <= 5)'),
        ).then(() =>
          knex.raw(onUpdateTrigger('appointment_rating')),
        ),
  ]);
};

exports.down = function(knex) {
  return Promise.all([
    knex.schema
        .dropTableIfExists('appointment')
        .dropTableIfExists('appointment_rating'),
  ]);
};
