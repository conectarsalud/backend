exports.up = function(knex) {
  return Promise.all([
    knex.schema.table('appointment', function(table) {
      table.integer('medical_specialty_id').notNullable().references('medical_specialty');
      table.string('reason', 250);
    }),
    knex.raw('ALTER TABLE appointment ALTER COLUMN doctor_id DROP NOT NULL'),
  ]);
};

exports.down = function(knex) {
  return Promise.all([
    knex.schema.hasColumn('appointment', 'medical_specialty_id').then(function(exists) {
      if (exists) {
        return knex.schema.table('appointment', function(table) {
          table.dropColumn('medical_specialty_id');
        });
      }
    }),
    knex.schema.hasColumn('appointment', 'reason').then(function(exists) {
      if (exists) {
        return knex.schema.table('appointment', function(table) {
          table.dropColumn('reason');
        });
      }
    }),
    knex.raw('ALTER TABLE appointment ALTER COLUMN doctor_id SET NOT NULL'),
  ]);
};
