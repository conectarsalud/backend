const { onUpdateTrigger } = require('../../knexfile.js');

exports.up = function(knex) {
  return Promise.all([
    knex.schema
        .createTable('medical_specialty', function(table) {
          table.increments('medical_specialty_id').unique().primary();
          table.string('name', 100).notNullable();
          table.timestamps(true, true);
        }).then(() =>
          knex.raw(onUpdateTrigger('medical_specialty')),
        ),
    knex.schema
        .createTable('doctor', function(table) {
          table.increments('doctor_id').unique().primary();
          table.string('name', 100).notNullable();
          table.string('medical_registration', 6).notNullable();
          table.enum('medical_registration_type', ['MN', 'MP']).notNullable();
          table.string('username', 100).unique().notNullable();
          table.string('password', 100).notNullable();
          table.string('specialties_csv');
          table.json('business_hours');
          table.timestamps(true, true);
        }).then(() =>
          knex.raw(onUpdateTrigger('doctor')),
        ),
    knex.schema
        .createTable('doctor_medical_specialty', function(table) {
          table.increments('doctor_medical_specialty_id').unique().primary();
          table.integer('doctor_id').notNullable().references('doctor');
          table.integer('medical_specialty_id').notNullable().references('medical_specialty');
          table.index('doctor_id', 'idx_doctor_medical_specialty_doctor_id');
          table.timestamps(true, true);
        }).then(() =>
          knex.raw(onUpdateTrigger('doctor_medical_specialty')),
        ),
  ]);
};

exports.down = function(knex) {
  return Promise.all([
    knex.schema
        .dropTableIfExists('medical_specialty')
        .dropTableIfExists('doctor')
        .dropTableIfExists('doctor_medical_specialty'),
  ]);
};
