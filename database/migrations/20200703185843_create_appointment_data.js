const { onUpdateTrigger } = require('../../knexfile.js');

exports.up = function(knex) {
  return Promise.all([
    knex.schema
      .createTable('appointment_data', function(table) {
        table.string('indications', 500).notNullable();
        table.integer('appointment_id').unique().primary().references('appointment');
        table.timestamps(true, true);
      }).then(() =>
      knex.raw(onUpdateTrigger('appointment_data')),
    )
  ]);
};

exports.down = function(knex) {
  return Promise.all([
    knex.schema
      .dropTableIfExists('appointment_data')
  ]);
};
