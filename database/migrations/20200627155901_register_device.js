const { onUpdateTrigger } = require('../../knexfile.js');

exports.up = function(knex) {
  return Promise.all([
    knex.schema
      .createTable('member_device_token', function(table) {
        table.string('device_token', 500).notNullable();
        table.integer('member_id').unique().primary().references('member');
        table.timestamps(true, true);
      }).then(() =>
      knex.raw(onUpdateTrigger('member_device_token')),
    )
  ]);
};

exports.down = function(knex) {
  return Promise.all([
    knex.schema
      .dropTableIfExists('member_device_token')
  ]);
};
