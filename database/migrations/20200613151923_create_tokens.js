const { onUpdateTrigger } = require('../../knexfile.js');

exports.up = function(knex) {
  return Promise.all([
    knex.schema
        .createTable('member_token', function(table) {
          table.string('token', 500).notNullable();
          table.integer('member_id').unique().primary().references('member');
          table.timestamps(true, true);
        }).then(() =>
          knex.raw(onUpdateTrigger('member_token')),
        ),
    knex.schema
        .createTable('doctor_token', function(table) {
          table.string('token', 500).notNullable();
          table.integer('doctor_id').unique().primary().references('doctor');
          table.timestamps(true, true);
        }).then(() =>
          knex.raw(onUpdateTrigger('doctor_token')),
        ),
  ]);
};

exports.down = function(knex) {
  return Promise.all([
    knex.schema
        .dropTableIfExists('member_token')
        .dropTableIfExists('doctor_token'),
  ]);
};
