const { onUpdateTrigger } = require('../../knexfile.js');

const ON_UPDATE_TIMESTAMP_FUNCTION = `
  CREATE OR REPLACE FUNCTION on_update_timestamp()
  RETURNS trigger AS $$
  BEGIN
    NEW.updated_at = now();
    RETURN NEW;
  END;
$$ language 'plpgsql';
`;

const DROP_ON_UPDATE_TIMESTAMP_FUNCTION = `DROP FUNCTION on_update_timestamp`;

exports.up = function(knex) {
  return Promise.all([
    knex.raw(ON_UPDATE_TIMESTAMP_FUNCTION),
    knex.schema
        .createTable('member', function(table) {
          table.increments('member_id').primary();
          table.string('name', 100);
          table.string('username', 100).unique().notNullable();
          table.string('password', 100).notNullable();
          table.string('dni', 10).notNullable();
          table.timestamps(true, true);
        }).then(() =>
          knex.raw(onUpdateTrigger('member')),
        ),
  ]);
};

exports.down = function(knex) {
  return Promise.all([
    knex.schema
        .dropTableIfExists('member')
        .then(() =>
          knex.raw(DROP_ON_UPDATE_TIMESTAMP_FUNCTION),
        ),
  ]);
};
