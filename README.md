# Conectar Salud Backend

This repository holds the backend for the Conectar Salud system for mobile medical appointments.

## Setup

The backend application is a NodeJS API using `express` library. To build locally you will need `node` and `npm` appropriate versions installed.

- `npm i`: install the project dependencies
- `npm test`: runs all the tests for the project
- `npm start`: starts the application locally

## Database

The underlying database is a PostgreSQL database provided by Heroku on the cloud.
The API connects to it using the `knex` library. All the related scripts to build the database and its content are provided.

### Mock structure

The `database/migrations` folder contains contains the structure of the mocked databases to be used.

### Mock content

The `database/seeds` folder contains the scripts for inserting the desired mock content on the databse.

## Continous integration

The project is integrated with CI provided by Gitlab. This means that everytime a new change is pushed, Gitlab executes a pipeline that builds the application and runs all the unit tests and displays the output. More details can be found on the `.gitlab-ci.yml` file.

## Continous delivery

The project is integrated with CD provided by Gitlab. This means that on specific branches, we will trigger automatic deployments to a Heroku application so that it can be used by the rest of the subsystems. . More details can be found on the `.gitlab-ci.yml` file.
