const knex = require('../../database/database.js').getConnection();

class MemberDeviceTokenModel {
  constructor(logger) {
    this.tableName = 'member_device_token';
    this.logger = logger;
  }

  async upsert(memberId, deviceToken) {
    const query = 'INSERT INTO member_device_token(member_id, device_token) VALUES (?, ?) ' +
      'ON CONFLICT(member_id) DO UPDATE ' +
      'SET device_token = excluded.device_token ' +
      'RETURNING member_id, device_token;';
    const values = [memberId, deviceToken];
    let upsertResponse;
    try {
      upsertResponse = await knex.raw(query, values);
    } catch (err) {
      this.logger.debug('MemberDeviceTokenModel - upsert - Error: %j', err);
    }
    if (Array.isArray(upsertResponse.rows) && upsertResponse.rows.length > 0) upsertResponse = upsertResponse.rows[0];
    this.logger.debug('MemberDeviceTokenModel - upsert - DB response: %j', upsertResponse);
    return upsertResponse;
  }

  async getByMemberId(memberId) {
    try {
      const selectResponse = await knex(this.tableName)
        .where('member_id', memberId)
        .first();
      this.logger.debug('MemberDeviceTokenModel - getByMemberId - DB response: %j', selectResponse);
      return selectResponse;
    } catch (err) {
      this.logger.debug('MemberDeviceTokenModel - getByMemberId - Error: %j', err);
    }
  }
}

module.exports = MemberDeviceTokenModel;
