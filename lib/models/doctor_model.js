const knex = require('../../database/database.js').getConnection();
const { APPOINTMENT_STATES } = require('../utils/constants.js');

class DoctorModel {
  constructor(logger) {
    this.doctorTable = 'doctor';
    this.memberTable = 'member';
    this.doctorSpecialtiesTable = 'doctor_medical_specialty';
    this.specialtiesTable = 'medical_specialty';
    this.appointmentsTable = 'appointment';
    this.appointmentsDataTable = 'appointment_data';
    this.logger = logger;
  }

  async getAllDoctors() {
    let selectResponse;
    try {
      selectResponse = await knex(this.doctorTable)
          .select(`${this.doctorTable}.doctor_id`,
              `${this.doctorTable}.name`,
              `${this.doctorTable}.medical_registration`,
              `${this.doctorTable}.specialties_csv`,
              `${this.doctorTable}.business_hours`,
          );
    } catch (err) {
      this.logger.debug('DoctorModel - getAllDoctors - Error: %j', err);
      throw err;
    }
    this.logger.debug('DoctorModel - getAllDoctors - DB response: %j', selectResponse);
    return selectResponse;
  }

  async getByUsername(username) {
    let selectResponse;
    try {
      selectResponse = await knex(this.doctorTable)
          .where('username', username);
    } catch (err) {
      this.logger.debug('DoctorModel - getByUsername - Error: %j', err);
      throw err;
    }
    if (Array.isArray(selectResponse) && selectResponse.length > 0) selectResponse = selectResponse[0];
    this.logger.debug('DoctorModel - getByUsername - DB response: %j', selectResponse);
    return selectResponse;
  }

  async getDoctorById(doctorId) {
    let selectResponse;
    try {
      selectResponse = await knex(this.doctorTable)
          .select(`${this.doctorTable}.doctor_id`,
              `${this.doctorTable}.name`,
              `${this.doctorTable}.medical_registration`,
              `${this.doctorTable}.specialties_csv`,
              `${this.doctorTable}.business_hours`)
          .where({ 'doctor_id': doctorId });
    } catch (err) {
      this.logger.debug('DoctorModel - getDoctorById - Error: %j', err);
      throw err;
    }
    if (Array.isArray(selectResponse) && selectResponse.length > 0) selectResponse = selectResponse[0];
    this.logger.debug('DoctorModel - getDoctorById - DB response: %j', selectResponse);
    return selectResponse;
  }

  async getDoctorSpecialties(doctorId) {
    let selectResponse;
    try {
      selectResponse = await knex(this.doctorSpecialtiesTable)
          .select(`${this.specialtiesTable}.name as specialty_name`)
          .innerJoin(this.specialtiesTable, `${this.doctorSpecialtiesTable}.medical_specialty_id`, `${this.specialtiesTable}.medical_specialty_id`)
          .where({ 'doctor_id': doctorId });
    } catch (err) {
      this.logger.debug('DoctorModel - getDoctorSpecialties - Error: %j', err);
      throw err;
    }
    this.logger.debug('DoctorModel - getDoctorSpecialties - DB response: %j', selectResponse);
    return selectResponse;
  }

  async getDoctorAppointments(doctorId) {
    let selectResponse;
    try {
      selectResponse = await knex(this.appointmentsTable)
          .select(`${this.appointmentsTable}.appointment_id as id`,
              `${this.appointmentsTable}.medical_specialty_id`,
              `${this.specialtiesTable}.name as specialty_name`,
              `${this.appointmentsTable}.reason`,
              `${this.appointmentsTable}.doctor_id`,
              `${this.appointmentsTable}.member_id`,
              `${this.memberTable}.name as member_name`,
              `${this.appointmentsTable}.status`,
              `${this.appointmentsTable}.connection_qr`,
              `${this.appointmentsTable}.created_at`,
              `${this.appointmentsDataTable}.indications`)
          .innerJoin(this.specialtiesTable, `${this.appointmentsTable}.medical_specialty_id`, `${this.specialtiesTable}.medical_specialty_id`)
          .innerJoin(this.memberTable, `${this.appointmentsTable}.member_id`, `${this.memberTable}.member_id`)
          .leftJoin(this.appointmentsDataTable, `${this.appointmentsTable}.appointment_id`, `${this.appointmentsDataTable}.appointment_id`)
          .where({ 'doctor_id': doctorId })
          .andWhere( `${this.appointmentsTable}.status`, APPOINTMENT_STATES.FINISHED )
          .orderBy(`${this.appointmentsTable}.created_at`, 'desc');
    } catch (err) {
      this.logger.debug('DoctorModel - getDoctorAppointments - Error: %j', err);
      throw err;
    }
    this.logger.debug('DoctorModel - getDoctorAppointments - DB response: %j', selectResponse);
    return selectResponse;
  }
}

module.exports = DoctorModel;
