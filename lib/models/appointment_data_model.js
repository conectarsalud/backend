const knex = require('../../database/database.js').getConnection();

class AppointmentDataModel {
  constructor(logger) {
    this.tableName = 'appointment_data';
    this.logger = logger;
  }

  async upsert(appointmentId, indications) {
    const query = 'INSERT INTO appointment_data(appointment_id, indications) VALUES (?, ?) ' +
      'ON CONFLICT(appointment_id) DO UPDATE ' +
      'SET indications = excluded.indications ' +
      'RETURNING appointment_id, indications;';
    const values = [appointmentId, indications];
    let upsertResponse;
    try {
      upsertResponse = await knex.raw(query, values);
    } catch (err) {
      this.logger.debug('AppointmentDataModel - upsert - Error: %j', err);
    }
    if (Array.isArray(upsertResponse.rows) && upsertResponse.rows.length > 0) upsertResponse = upsertResponse.rows[0];
    this.logger.debug('AppointmentDataModel - upsert - DB response: %j', upsertResponse);
    return upsertResponse;
  }
}

module.exports = AppointmentDataModel;
