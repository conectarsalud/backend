const knex = require('../../database/database.js').getConnection();

class MemberTokenModel {
  constructor(logger) {
    this.tableName = 'member_token';
    this.logger = logger;
  }

  async upsert(memberObject, tokenObject) {
    const query = 'INSERT INTO member_token(member_id, token) VALUES (?, ?) ' +
      'ON CONFLICT(member_id) DO UPDATE ' +
      'SET token = excluded.token ' +
      'RETURNING member_id, token;';
    const values = [memberObject.member_id, tokenObject.token];
    let upsertResponse;
    try {
      upsertResponse = await knex.raw(query, values);
    } catch (err) {
      this.logger.debug('MemberTokenModel - upsert - Error: %j', err);
    }
    if (Array.isArray(upsertResponse.rows) && upsertResponse.rows.length > 0) upsertResponse = upsertResponse.rows[0];
    this.logger.debug('MemberTokenModel - upsert - DB response: %j', upsertResponse);
    return upsertResponse;
  }

  async getByMemberId(memberId) {
    try {
      const selectResponse = await knex(this.tableName)
          .where('member_id', memberId)
          .first();
      this.logger.debug('MemberTokenModel - getByMemberId - DB response: %j', selectResponse);
      return selectResponse;
    } catch (err) {
      this.logger.debug('MemberTokenModel - getByMemberId - Error: %j', err);
    }
  }
}

module.exports = MemberTokenModel;
