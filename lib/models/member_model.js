const knex = require('../../database/database.js').getConnection();
const { APPOINTMENT_STATES } = require('../utils/constants.js');

class MemberModel {
  constructor(logger) {
    this.membersTable = 'member';
    this.doctorsTable = 'doctor';
    this.appointmentsTable = 'appointment';
    this.appointmentsDataTable = 'appointment_data';
    this.specialtiesTable = 'medical_specialty';
    this.logger = logger;
  }

  async getByUsername(username) {
    let selectResponse;
    try {
      selectResponse = await knex(this.membersTable)
          .where('username', username);
    } catch (err) {
      this.logger.debug('MemberModel - getByUsername - Error: %j', err);
      throw err;
    }
    if (Array.isArray(selectResponse) && selectResponse.length > 0) selectResponse = selectResponse[0];
    this.logger.debug('MemberModel - getByUsername - DB response: %j', selectResponse);
    return selectResponse;
  }

  async getMemberById(memberId) {
    let selectResponse;
    try {
      selectResponse = await knex(this.membersTable)
          .select(`${this.membersTable}.member_id`,
              `${this.membersTable}.dni`,
              `${this.membersTable}.name`)
          .where({ 'member_id': memberId });
    } catch (err) {
      this.logger.debug('MemberModel- getMemberById - Error: %j', err);
      throw err;
    }
    if (Array.isArray(selectResponse) && selectResponse.length > 0) selectResponse = selectResponse[0];
    this.logger.debug('MemberModel - getMemberById - DB response: %j', selectResponse);
    return selectResponse;
  }

  async getMemberAppointments(memberId) {
    let selectResponse;
    try {
      selectResponse = await knex(this.appointmentsTable)
          .select(`${this.appointmentsTable}.appointment_id as id`,
              `${this.appointmentsTable}.medical_specialty_id`,
              `${this.specialtiesTable}.name as specialty_name`,
              `${this.appointmentsTable}.reason`,
              `${this.appointmentsTable}.doctor_id`,
              `${this.doctorsTable}.name as doctor_name`,
              `${this.appointmentsTable}.member_id`,
              `${this.appointmentsTable}.status`,
              `${this.appointmentsTable}.created_at`,
              `${this.appointmentsDataTable}.indications`)
          .innerJoin(this.specialtiesTable, `${this.appointmentsTable}.medical_specialty_id`, `${this.specialtiesTable}.medical_specialty_id`)
          .innerJoin(this.doctorsTable, `${this.appointmentsTable}.doctor_id`, `${this.doctorsTable}.doctor_id`)
          .leftJoin(this.appointmentsDataTable, `${this.appointmentsTable}.appointment_id`, `${this.appointmentsDataTable}.appointment_id`)
          .where(`${this.appointmentsTable}.member_id`, memberId)
          .andWhere( `${this.appointmentsTable}.status`, APPOINTMENT_STATES.FINISHED )
          .orderBy(`${this.appointmentsTable}.created_at`, 'desc');
    } catch (err) {
      this.logger.debug('MemberModel - getMemberAppointments - Error: %j', err);
      throw err;
    }
    this.logger.debug('MemberModel - getMemberAppointments - DB response: %j', selectResponse);
    return selectResponse;
  }
}

module.exports = MemberModel;
