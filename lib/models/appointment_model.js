const knex = require('../../database/database.js').getConnection();
const { APPOINTMENT_STATES } = require('../utils/constants.js');

class AppointmentModel {
  constructor(logger) {
    this.appointmentTable = 'appointment';
    this.memberTable = 'member';
    this.specialtiesTable = 'medical_specialty';
    this.logger = logger;
  }

  async getMedicalSpecialties() {
    let selectResponse;
    try {
      selectResponse = await knex(this.specialtiesTable)
          .select(`${this.specialtiesTable}.medical_specialty_id`,
              `${this.specialtiesTable}.name`,
          );
    } catch (err) {
      this.logger.debug('AppointmentModel - getMedicalSpecialties - Error: %j', err);
      throw err;
    }
    this.logger.debug('AppointmentModel - getMedicalSpecialties - DB response: %j', selectResponse);
    return selectResponse;
  }

  async getMedicalSpecialty(medicalSpecialtyId) {
    let selectResponse;
    try {
      selectResponse = await knex(this.specialtiesTable)
          .select(`${this.specialtiesTable}.medical_specialty_id`,
              `${this.specialtiesTable}.name`)
          .where({ 'medical_specialty_id': medicalSpecialtyId })
          .first();
    } catch (err) {
      this.logger.debug('AppointmentModel - getMedicalSpecialty - Error: %j', err);
      throw err;
    }
    if (Array.isArray(selectResponse) && selectResponse.length > 0) selectResponse = selectResponse[0];
    this.logger.debug('AppointmentModel - getMedicalSpecialty - DB response: %j', selectResponse);
    return selectResponse;
  }

  async createNew(memberId, status, channelId, medicalSpecialtyId, reason) {
    const appointment = {
      member_id: memberId,
      status: status,
      channel_id: channelId,
      medical_specialty_id: medicalSpecialtyId,
      reason: reason,
    };
    try {
      const insertResponse = await knex(this.appointmentTable)
          .insert(appointment)
          .returning('*');
      this.logger.debug('AppointmentModel - createNew - DB response: %j', insertResponse);
      return Array.isArray(insertResponse)? insertResponse[0] : insertResponse;
    } catch (err) {
      this.logger.debug('AppointmentModel - createNew - Error: %j', err);
      throw err;
    }
  }

  async update(appointmentObject) {
    try {
      const updateResponse = await knex(this.appointmentTable)
          .where('appointment_id', appointmentObject.appointment_id)
          .update({
            'doctor_id': appointmentObject.doctor_id,
            'connection_qr': appointmentObject.connection_qr,
            'status': appointmentObject.status,
            'member_last_connection_time': appointmentObject.member_last_connection_time,
            'doctor_last_connection_time': appointmentObject.doctor_last_connection_time,
            'conference_start_time': appointmentObject.conference_start_time,
            'conference_end_time': appointmentObject.conference_end_time,
          })
          .returning('*');
      this.logger.debug('AppointmentModel - update - DB response: %j', updateResponse);
      return Array.isArray(updateResponse)? updateResponse[0] : updateResponse;
    } catch (err) {
      this.logger.debug('AppointmentModel - update - Error: %j', err);
      throw err;
    }
  }

  async getAppointmentById(appointmentId) {
    let selectResponse;
    try {
      selectResponse = await knex(this.appointmentTable)
          .select(`${this.appointmentTable}.appointment_id`,
              `${this.appointmentTable}.medical_specialty_id`,
              `${this.specialtiesTable}.name as specialty_name`,
              `${this.appointmentTable}.reason`,
              `${this.appointmentTable}.doctor_id`,
              `${this.appointmentTable}.member_id`,
              `${this.appointmentTable}.channel_id`,
              `${this.appointmentTable}.status`,
              `${this.appointmentTable}.connection_qr`,
              `${this.appointmentTable}.member_last_connection_time`,
              `${this.appointmentTable}.doctor_last_connection_time`,
              `${this.appointmentTable}.conference_start_time`,
              `${this.appointmentTable}.conference_end_time`,
              `${this.appointmentTable}.created_at`,
          )
          .where({ 'appointment_id': appointmentId })
          .innerJoin(this.specialtiesTable, `${this.appointmentTable}.medical_specialty_id`, `${this.specialtiesTable}.medical_specialty_id`)
          .first();
    } catch (err) {
      this.logger.debug('AppointmentModel - getAppointmentById - Error: %j', err);
      throw err;
    }
    if (Array.isArray(selectResponse) && selectResponse.length > 0) selectResponse = selectResponse[0];
    this.logger.debug('AppointmentModel - getAppointmentById - DB response: %j', selectResponse);
    return selectResponse;
  }

  async getQueuedAppointments() {
    let selectResponse;
    try {
      selectResponse = await knex(this.appointmentTable)
          .select(`${this.appointmentTable}.appointment_id as id`,
              `${this.appointmentTable}.member_id`,
              `${this.memberTable}.name as member_name`,
              `${this.specialtiesTable}.name as specialty_name`,
              `${this.appointmentTable}.reason`,
              `${this.appointmentTable}.status`,
              `${this.appointmentTable}.connection_qr`,
              `${this.appointmentTable}.created_at`,
          )
          .where({ 'status': APPOINTMENT_STATES.QUEUED })
          .innerJoin(this.specialtiesTable, `${this.appointmentTable}.medical_specialty_id`, `${this.specialtiesTable}.medical_specialty_id`)
          .innerJoin(this.memberTable, `${this.appointmentTable}.member_id`, `${this.memberTable}.member_id`)
          .orderBy(`${this.appointmentTable}.created_at`, 'asc');
    } catch (err) {
      this.logger.debug('AppointmentModel - getQueuedAppointments - Error: %j', err);
      throw err;
    }
    this.logger.debug('AppointmentModel - getQueuedAppointments - DB response: %j', selectResponse);
    return selectResponse;
  }

  async getWaitingMembersCount(appointment) {
    let selectResponse;
    try {
      selectResponse = await knex(this.appointmentTable)
          .countDistinct(`${this.appointmentTable}.appointment_id as waiting_members_count`)
          .where({ 'status': APPOINTMENT_STATES.QUEUED })
          .andWhere('created_at', '<', appointment.created_at)
          .andWhereNot( 'appointment_id', appointment.appointment_id );
    } catch (err) {
      this.logger.debug('AppointmentModel - getWaitingMembersCount - Error: %j', err);
      throw err;
    }
    if (Array.isArray(selectResponse) && selectResponse.length > 0) selectResponse = selectResponse[0];
    this.logger.debug('AppointmentModel - getWaitingMembersCount - DB response: %j', selectResponse);
    return selectResponse;
  }
}

module.exports = AppointmentModel;
