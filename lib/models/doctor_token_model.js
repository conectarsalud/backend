const knex = require('../../database/database.js').getConnection();

class DoctorTokenModel {
  constructor(logger) {
    this.tableName = 'doctor_token';
    this.logger = logger;
  }

  async upsert(doctorObject, tokenObject) {
    const query = 'INSERT INTO doctor_token(doctor_id, token) VALUES (?, ?) ' +
      'ON CONFLICT(doctor_id) DO UPDATE ' +
      'SET token = excluded.token ' +
      'RETURNING doctor_id, token;';
    const values = [doctorObject.doctor_id, tokenObject.token];
    let upsertResponse;
    try {
      upsertResponse = await knex.raw(query, values);
    } catch (err) {
      this.logger.debug('DoctorTokenModel - upsert - Error: %j', err);
    }
    if (Array.isArray(upsertResponse.rows) && upsertResponse.rows.length > 0) upsertResponse = upsertResponse.rows[0];
    this.logger.debug('DoctorTokenModel - upsert - DB response: %j', upsertResponse);
    return upsertResponse;
  }

  async getByDoctorId(doctorId) {
    try {
      const selectResponse = await knex(this.tableName)
          .where('doctor_id', doctorId)
          .first();
      this.logger.debug('DoctorTokenModel - getByDoctorId - DB response: %j', selectResponse);
      return selectResponse;
    } catch (err) {
      this.logger.debug('DoctorTokenModel - getByDoctorId - Error: %j', err);
    }
  }
}

module.exports = DoctorTokenModel;
