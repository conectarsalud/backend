const knex = require('../../database/database.js').getConnection();

class AppointmentRatingModel {
  constructor(logger) {
    this.appointmentRatingTable = 'appointment_rating';
    this.logger = logger;
  }

  async createNew(appointmentId, rating, comments) {
    const appointmentRating = {
      appointment_id: appointmentId,
      rating: rating,
      comments: comments,
    };
    try {
      const insertResponse = await knex(this.appointmentRatingTable)
          .insert(appointmentRating)
          .returning('*');
      this.logger.debug('AppointmentRatingModel - createNew - DB response: %j', insertResponse);
      return Array.isArray(insertResponse)? insertResponse[0] : insertResponse;
    } catch (err) {
      this.logger.debug('AppointmentRatingModel - createNew - Error: %j', err);
      throw err;
    }
  }
}

module.exports = AppointmentRatingModel;
