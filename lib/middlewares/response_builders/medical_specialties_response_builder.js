function MedicalSpecialtiesResponseBuilder(logger) {
  const _logger = logger;

  this.buildResponse = function(req, res) {
    const response = {};
    response.medical_specialties = res.medicalSpecialties.map((medicalSpecialty) => ({
      medical_specialty_id: medicalSpecialty.medical_specialty_id,
      name: medicalSpecialty.name,
    }));
    _logger.debug('Response: %j', response);
    res.status(200).json(response);
  };
}

module.exports = MedicalSpecialtiesResponseBuilder;
