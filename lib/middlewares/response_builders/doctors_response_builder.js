function DoctorsResponseBuilder(logger) {
  const _logger = logger;

  this.buildResponse = function(req, res) {
    const response = {};
    response.doctors = res.doctors.map((doctor) => ({
      doctor_id: doctor.doctor_id,
      nombre: doctor.name,
      matricula: doctor.medical_registration,
      especialidades: doctor.specialties_csv,
      horario_atencion: doctor.business_hours.map((businessHour) => (
        { dia: businessHour.day, hora_inicio: businessHour.start_time, hora_fin: businessHour.end_time })),
      disponible: doctor.isAvailable? 'si' : 'no',
    }));
    _logger.debug('Response: %j', response);
    res.status(200).json(response);
  };
}

module.exports = DoctorsResponseBuilder;
