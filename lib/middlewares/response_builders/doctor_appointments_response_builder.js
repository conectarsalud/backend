const { convertFromUtcToLocal } = require('../../utils/date_converter.js');

function DoctorAppointmentsResponseBuilder(logger) {
  const _logger = logger;

  const statusTranslations = {
    'Queued': 'En cola',
    'Assigned': 'Asignado',
    'Waiting member': 'Esperando afiliado',
    'Waiting doctor': 'Esperando profesional',
    'In progress': 'En curso',
    'Member disconnect': 'En curso',
    'Doctor disconnect': 'En curso',
    'Finished': 'Terminado',
  };

  this.buildResponse = function(req, res) {
    const response = {};
    response.appointments = res.appointments.map((appointment) => ({
      id: appointment.id,
      especialidad: appointment.specialty_name,
      doctor_id: appointment.doctor_id,
      member: {
        member_id: appointment.member_id,
        nombre: appointment.member_name,
      },
      estado: statusTranslations[appointment.status],
      motivo: appointment.reason,
      hora_solicitud: convertFromUtcToLocal(appointment.created_at),
      informacion: {
        indicaciones: appointment.indications,
      }
    }));
    _logger.debug('Response: %j', response);
    res.status(200).json(response);
  };
}

module.exports = DoctorAppointmentsResponseBuilder;
