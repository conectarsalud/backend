function QueuedAppointmentsResponseBuilder(logger) {
  const _logger = logger;

  const statusTranslations = {
    'Queued': 'En cola',
    'Assigned': 'Asignado',
    'Waiting member': 'Esperando afiliado',
    'Waiting doctor': 'Esperando profesional',
    'In progress': 'En curso',
    'Member disconnect': 'En curso',
    'Doctor disconnect': 'En curso',
    'Finished': 'Terminado',
  };

  this.buildResponse = function(req, res) {
    const response = {};
    res.queuedAppointments.sort((a, b) => b.total_wait_time_seconds - a.total_wait_time_seconds);
    response.queued_appointments = res.queuedAppointments.map((appointment) => ({
      id: appointment.id,
      especialidad: appointment.specialty_name,
      member: {
        member_id: appointment.member_id,
        nombre: appointment.member_name,
      },
      estado: statusTranslations[appointment.status],
      qr_conexion: appointment.connection_qr,
      motivo: appointment.reason,
      hora_solicitud: appointment.created_at,
      tiempo_espera: appointment.wait_time,
    }));
    _logger.debug('Response: %j', response);
    res.status(200).json(response);
  };
}

module.exports = QueuedAppointmentsResponseBuilder;
