function AppointmentResponseBuilder(logger) {
  const _logger = logger;

  this.buildResponse = function(req, res) {
    const response = {};
    response.appointment_rating = {
      appointment_id: res.appointment_rating.appointment_id,
      rating: res.appointment_rating.rating,
      comments: res.appointment_rating.comments,
    };
    _logger.debug('Response: %j', response);
    res.status(200).json(response);
  };
}

module.exports = AppointmentResponseBuilder;
