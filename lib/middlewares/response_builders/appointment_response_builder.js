const { APPOINTMENT_STATES } = require('../../utils/constants.js');
const { convertFromUtcToLocal } = require('../../utils/date_converter.js');

function AppointmentResponseBuilder(logger) {
  const _logger = logger;

  const statusTranslations = {
    [APPOINTMENT_STATES.QUEUED]: 'En cola',
    [APPOINTMENT_STATES.ASSIGNED]: 'Asignado',
    [APPOINTMENT_STATES.WAITING_MEMBER]: 'Esperando afiliado',
    [APPOINTMENT_STATES.WAITING_DOCTOR]: 'Esperando profesional',
    [APPOINTMENT_STATES.IN_PROGRESS]: 'En curso',
    [APPOINTMENT_STATES.MEMBER_DISCONNECT]: 'En curso',
    [APPOINTMENT_STATES.DOCTOR_DISCONNECT]: 'En curso',
    [APPOINTMENT_STATES.FINISHED]: 'Terminado',
  };

  this.buildResponse = function(req, res) {
    const response = {};
    response.appointment = {
      id: res.appointment.appointment_id,
      especialidad: res.appointment.medical_specialty,
      member_id: res.appointment.member_id,
      estado: statusTranslations[res.appointment.status],
      channel_id: res.appointment.channel_id,
      motivo: res.appointment.reason,
      hora_solicitud: convertFromUtcToLocal(res.appointment.created_at),
      personas_en_espera: res.appointment.waiting_members_count,
    };
    if (res.appointmentData){
      response.appointment.informacion = {
        indicaciones: res.appointmentData.indications,
      }
    }
    _logger.debug('Response: %j', response);
    res.status(200).json(response);
  };
}

module.exports = AppointmentResponseBuilder;
