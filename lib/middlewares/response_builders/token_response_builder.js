function TokenResponseBuilder(logger) {
  const _logger = logger;

  this.buildResponse = function(req, res) {
    const response = {};
    response.token = res.token;
    _logger.debug('Response: %j', response);
    res.status(200).json(response);
  };
}

module.exports = TokenResponseBuilder;
