function MemberDeviceRegistrationResponseBuilder(logger) {
  const _logger = logger;

  this.buildResponse = function(req, res) {
    const response = {};
    response.member_device_token = res.memberDeviceToken;
    _logger.debug('Response: %j', response);
    res.status(200).json(response);
  };
}

module.exports = MemberDeviceRegistrationResponseBuilder;
