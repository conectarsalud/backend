const config = require('config');
const MemberService = require('../../services/member_service.js');
const AppointmentService = require('../../services/appointment_service.js');
const NotificationService = require('../../services/notification_service.js');
const MemberTokenService = require('../../services/member_token_service.js');
const MemberDeviceTokenService = require('../../services/member_device_token_service.js');
const { MEMBER_NOTIFICATIONS_MESSAGES } = require('../../utils/constants.js');
const BaseHttpError = require('../../errors/base_http_error.js');

function MemberController(logger) {
  const _memberService = new MemberService(logger);
  const _appointmentService = new AppointmentService(logger);
  const _notificationService = new NotificationService(logger);
  const _memberTokenService = new MemberTokenService(logger);
  const _memberDeviceTokenService = new MemberDeviceTokenService(logger);

  this.generateToken = async (req, res, next) => {
    const member = res.member;
    let token;
    try {
      token = await _memberTokenService.generateToken(member);
    } catch (err) {
      logger.error(`An error occurred while generating the token for member: ${member.member_id}`);
      return next(err);
    }
    res.token = token;
    return next();
  };

  this.getAppointments = async (req, res, next) => {
    const memberId = res.memberAuthenticated.member_id;
    let appointments;
    try {
      appointments = await _memberService.getAppointments(memberId);
    } catch (err) {
      const error = new BaseHttpError('Member appointments retrieval error', 500);
      return next(error);
    }
    res.appointments = appointments;
    return next();
  };

  this.registerDevice = async (req, res, next) => {
    const memberId = res.memberAuthenticated.member_id;
    const deviceToken = req.body.device_token;
    let memberDeviceToken;
    try {
      memberDeviceToken = await _memberDeviceTokenService.registerDevice(memberId, deviceToken);
    } catch (err) {
      const error = new BaseHttpError('Member device registration error', 500);
      return next(error);
    }
    res.memberDeviceToken = memberDeviceToken;
    return next();
  };

  this.sendNotificationsOnAppointmentStart = (req, res, next) => {
    if (config.SEND_MEMBER_NOTIFICATIONS) {
      this.sendNotificationToAppointmentMember(res);
      this.sendNotificationsToMembersInQueue();
    }
    return next();
  }

  this.sendNotificationToAppointmentMember = (res) => {
    const memberId = res.appointment.member_id;
    const appointmentId = res.appointment.appointment_id;
    const notificationMessage = MEMBER_NOTIFICATIONS_MESSAGES.APPOINTMENT_START;
    //don't wait for response
    _notificationService.sendNotificationToMember(memberId, notificationMessage, appointmentId);
  }

  this.sendNotificationsToMembersInQueue = async () => {
    let queuedAppointments;
    try{
      queuedAppointments = await _appointmentService.getQueuedAppointments();
      if (queuedAppointments.length > 5){
        this.sendNotificationToMemberNumberSix(queuedAppointments);
        this.sendNotificationToNextMember(queuedAppointments)
      } else if ((queuedAppointments.length > 0)){
        this.sendNotificationToNextMember(queuedAppointments)
      }
    } catch (err){
      logger.error('Queued appointments retrieval error');
    }
  }

  this.sendNotificationToMemberNumberSix = (queuedAppointments) => {
    const memberIdNumberSixInQueue = queuedAppointments[5].member_id;
    const appointmentId = queuedAppointments[5].id;
    const notificationMessage = MEMBER_NOTIFICATIONS_MESSAGES.FIVE_IN_QUEUE;
    //don't wait for response
    _notificationService.sendNotificationToMember(memberIdNumberSixInQueue, notificationMessage, appointmentId);
  }

  this.sendNotificationToNextMember = (queuedAppointments) => {
    const memberIdNextInQueue = queuedAppointments[0].member_id;
    const appointmentId = queuedAppointments[0].id;
    const notificationMessage = MEMBER_NOTIFICATIONS_MESSAGES.NEXT_IN_QUEUE;
    //don't wait for response
    _notificationService.sendNotificationToMember(memberIdNextInQueue, notificationMessage, appointmentId);
  }
}

module.exports = MemberController;
