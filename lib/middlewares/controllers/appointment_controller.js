const moment = require('moment');
const { APPOINTMENT_STATES } = require('../../utils/constants.js');
const { getCurrentLocalTime, convertFromUtcToLocal } = require('../../utils/date_converter.js');
const AppointmentService = require('../../services/appointment_service.js');
const QrGeneratorService = require('../../services/qr_generator_service.js');
const BaseHttpError = require('../../errors/base_http_error.js');

function AppointmentController(logger) {
  const _appointmentService = new AppointmentService(logger);
  const _qrGeneratorService = new QrGeneratorService(logger);

  this.getMedicalSpecialties = async (req, res, next) => {
    let medicalSpecialties;
    try {
      medicalSpecialties = await _appointmentService.getMedicalSpecialties();
    } catch (err) {
      const error = new BaseHttpError('Medical specialties retrieval error', 500);
      return next(error);
    }
    res.medicalSpecialties = medicalSpecialties;
    return next();
  };

  this.getMedicalSpecialtyForAppointment = async (req, res, next) => {
    const medicalSpecialtyId = res.appointment.medical_specialty_id;
    let medicalSpecialty;
    try {
      medicalSpecialty = await _appointmentService.getMedicalSpecialty(medicalSpecialtyId);
    } catch (err) {
      const error = new BaseHttpError('Medical specialty retrieval error', 500);
      return next(error);
    }
    res.appointment.medical_specialty = medicalSpecialty;
    return next();
  };

  this.requestAppointment = async (req, res, next) => {
    const memberId = res.memberAuthenticated.member_id;
    const medicalSpecialty = req.body.medical_specialty_id;
    const reason = req.body.reason;
    let appointment;
    try {
      appointment = await _appointmentService.newAppointment(memberId, medicalSpecialty, reason);
    } catch (err) {
      const error = new BaseHttpError('Appointment creation error', 500);
      return next(error);
    }
    res.appointment = appointment;
    return next();
  };

  this.generateQr = async (req, res, next) => {
    const appointment = res.appointment;
    const qrContent = {
      appointment_id: appointment.appointment_id,
      channel_id: appointment.channel_id,
    };
    let qrResource;
    try {
      qrResource = await _qrGeneratorService.generateQrAndUpload(qrContent);
    } catch (err) {
      const error = new BaseHttpError('Connection qr creation error', 500);
      return next(error);
    }
    res.appointment.connection_qr = qrResource;
    await _appointmentService.updateAppointment(res.appointment);
    return next();
  };

  this.getAppointment = async (req, res, next) => {
    const appointmentId = req.params.appointmentId;
    let appointment;
    try {
      appointment = await _appointmentService.retrieveAppointment(appointmentId);
    } catch (err) {
      const error = new BaseHttpError('Appointment find error', 500);
      return next(error);
    }
    if (appointment) {
      res.appointment = appointment;
      return next();
    } else {
      const error = new BaseHttpError('Appointment not found', 404);
      return next(error);
    }
  };

  this.updateAppointmentOnMemberConnection = async (req, res, next) => {
    const appointment = res.appointment;
    let updatedAppointment;
    try {
      updatedAppointment = updateStateAndTimeOnMemberConnection(appointment);
    } catch (err) {
      const error = new BaseHttpError('Invalid appointment action', 500);
      return next(error);
    }
    return await updateAppointment(updatedAppointment, res, next);
  };

  async function updateAppointment(updatedAppointment, res, next) {
    try {
      await _appointmentService.updateAppointment(updatedAppointment);
    } catch (err) {
      const error = new BaseHttpError('Appointment update error', 500);
      return next(error);
    }
    res.appointment = updatedAppointment;
    return next();
  }

  this.updateAppointmentOnMemberDisconnection = async (req, res, next) => {
    const appointment = res.appointment;
    let updatedAppointment;
    try {
      updatedAppointment = updateStateAndTimeOnMemberDisconnection(appointment);
    } catch (err) {
      const error = new BaseHttpError('Invalid appointment action', 500);
      return next(error);
    }
    return await updateAppointment(updatedAppointment, res, next);
  };

  function updateStateAndTimeOnMemberConnection(app) {
    const currentTime = moment();
    const appointment = app;
    let newState;
    switch (appointment.status) {
      case APPOINTMENT_STATES.WAITING_MEMBER:
        newState = APPOINTMENT_STATES.IN_PROGRESS;
        appointment.conference_start_time = currentTime;
        break;
      case APPOINTMENT_STATES.ASSIGNED:
        newState = APPOINTMENT_STATES.WAITING_DOCTOR;
        break;
      case APPOINTMENT_STATES.IN_PROGRESS:
        newState = APPOINTMENT_STATES.IN_PROGRESS;
        break;
      default:
        throw new Error('Invalid appointment state transition');
    }
    appointment.status = newState;
    appointment.member_last_connection_time = currentTime;
    return appointment;
  }

  function updateStateAndTimeOnMemberDisconnection(app) {
    const currentTime = moment();
    const appointment = app;
    let newState;
    switch (appointment.status) {
      case APPOINTMENT_STATES.DOCTOR_DISCONNECT:
        newState = APPOINTMENT_STATES.FINISHED;
        appointment.conference_end_time = currentTime;
        break;
      case APPOINTMENT_STATES.IN_PROGRESS:
        newState = APPOINTMENT_STATES.MEMBER_DISCONNECT;
        break;
      default:
        throw new Error('Invalid appointment state transition');
    }
    appointment.status = newState;
    return appointment;
  }

  this.updateAppointmentOnDoctorConnection = async (req, res, next) => {
    const appointment = res.appointment;
    let updatedAppointment;
    try {
      updatedAppointment = updateStateAndTimeOnDoctorConnection(appointment);
    } catch (err) {
      const error = new BaseHttpError('Invalid appointment action', 500);
      return next(error);
    }
    return await updateAppointment(updatedAppointment, res, next);
  };

  this.updateAppointmentOnDoctorDisconnection = async (req, res, next) => {
    const appointment = res.appointment;
    let updatedAppointment;
    try {
      updatedAppointment = updateStateAndTimeOnDoctorDisconnection(appointment);
    } catch (err) {
      const error = new BaseHttpError('Invalid appointment action', 500);
      return next(error);
    }
    return await updateAppointment(updatedAppointment, res, next);
  };

  function updateStateAndTimeOnDoctorConnection(app) {
    const currentTime = moment();
    const appointment = app;
    let newState;
    switch (appointment.status) {
      case APPOINTMENT_STATES.WAITING_DOCTOR:
        newState = APPOINTMENT_STATES.IN_PROGRESS;
        appointment.conference_start_time = currentTime;
        break;
      case APPOINTMENT_STATES.ASSIGNED:
        newState = APPOINTMENT_STATES.WAITING_MEMBER;
        break;
      case APPOINTMENT_STATES.IN_PROGRESS:
        newState = APPOINTMENT_STATES.IN_PROGRESS;
        break;
      default:
        throw new Error('Invalid appointment state transition');
    }
    appointment.status = newState;
    appointment.doctor_last_connection_time = currentTime;
    return appointment;
  }

  function updateStateAndTimeOnDoctorDisconnection(app) {
    const currentTime = moment();
    const appointment = app;
    let newState;
    switch (appointment.status) {
      case APPOINTMENT_STATES.MEMBER_DISCONNECT:
        newState = APPOINTMENT_STATES.FINISHED;
        appointment.conference_end_time = currentTime;
        break;
      case APPOINTMENT_STATES.IN_PROGRESS:
        newState = APPOINTMENT_STATES.DOCTOR_DISCONNECT;
        break;
      default:
        throw new Error('Invalid appointment state transition');
    }
    appointment.status = newState;
    return appointment;
  }

  this.rateAppointment = async (req, res, next) => {
    const appointmentId = req.params.appointmentId;
    const rating = req.body.rating;
    const comments = req.body.comments;
    let appointmentRating;
    try {
      appointmentRating = await _appointmentService.rateAppointment(appointmentId, rating, comments);
    } catch (err) {
      const error = new BaseHttpError('Appointment rating creation error', 500);
      return next(error);
    }
    res.appointment_rating = appointmentRating;
    return next();
  };

  this.assignAppointmentToDoctor = async (req, res, next) => {
    const doctorId = res.doctorAuthenticated.doctor_id;
    const appointment = res.appointment;
    switch (appointment.status) {
      case APPOINTMENT_STATES.QUEUED:
        appointment.doctor_id = doctorId;
        appointment.status = APPOINTMENT_STATES.ASSIGNED;
        break;
      default:
        const error = new BaseHttpError(`Not able to assign appointment to doctor`, 500);
        return next(error);
    }
    return await updateAppointment(appointment, res, next);
  };

  this.checkAppointmentOwnership = async (req, res, next) => {
    const doctorId = res.doctorAuthenticated.doctor_id;
    const appointment = res.appointment;
    if (appointment.doctor_id != doctorId){
      const error = new BaseHttpError(`Another doctor has already answered the appointment`, 500);
      return next(error);
    }
    return next();
  };

  this.getQueuedAppointments = async (req, res, next) => {
    let queuedAppointments;
    try {
      queuedAppointments = await _appointmentService.getQueuedAppointments();
    } catch (err) {
      const error = new BaseHttpError('Queued appointments retrieval error', 500);
      return next(error);
    }
    res.queuedAppointments = queuedAppointments;
    return next();
  };

  this.calculateMemberWaitTime = async (req, res, next) => {
    const currentTime = moment(getCurrentLocalTime());
    const queuedAppointments = res.queuedAppointments;

    // update all the appointments with the wait time
    const updatedAppointmentsPromises = queuedAppointments.map(async (appointment) => {
      const convertedCreationTime = convertFromUtcToLocal(appointment.created_at);
      appointment.created_at = convertedCreationTime;
      const appointmentCreationTime = moment(convertedCreationTime);

      // calculate total duration
      const totalWaitTimeDuration = moment.duration(currentTime.diff(appointmentCreationTime));

      const waitTimeHours = parseInt(totalWaitTimeDuration.asHours());
      const waitTimeMinutes = parseInt(totalWaitTimeDuration.asMinutes()) % 60;
      const totalWaitTimeSeconds = parseInt(totalWaitTimeDuration.asSeconds());
      const waitTimeSeconds = totalWaitTimeSeconds % 60;

      appointment.total_wait_time_seconds = totalWaitTimeSeconds;
      appointment.wait_time = `${waitTimeHours} horas ${waitTimeMinutes} minutos ${waitTimeSeconds} segundos`;
      return appointment;
    });

    let updatedAppointments;
    try {
      updatedAppointments = await Promise.all(updatedAppointmentsPromises);
    } catch (err) {
      const error = new BaseHttpError('Wait time update error', 500);
      return next(error);
    }

    res.queuedAppointments = updatedAppointments;
    return next();
  };

  this.calculateWaitingMembers = async (req, res, next) => {
    const appointment = res.appointment;
    let waitingMembersCount;
    try {
      waitingMembersCount = await _appointmentService.getWaitingMembersCount(appointment);
    } catch (err) {
      const error = new BaseHttpError('Waiting members retrieval error', 500);
      return next(error);
    }
    appointment.waiting_members_count = waitingMembersCount.waiting_members_count;
    res.appointment = appointment;
    return next();
  };

  this.saveAppointmentData = async (req, res, next) => {
    const appointmentId = req.params.appointmentId;;
    const indications = req.body.indications;
    let appointmentData;
    try {
      appointmentData = await _appointmentService.saveAppointmentData(appointmentId, indications);
    } catch (err) {
      const error = new BaseHttpError('Appointment data update error', 500);
      return next(error);
    }
    res.appointmentData = appointmentData;
    return next();
  };
}

module.exports = AppointmentController;
