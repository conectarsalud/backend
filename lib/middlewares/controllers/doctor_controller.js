const moment = require('moment');
const DoctorService = require('../../services/doctor_service.js');
const DoctorTokenService = require('../../services/doctor_token_service.js');
const BaseHttpError = require('../../errors/base_http_error.js');

function DoctorController(logger) {
  const _doctorService = new DoctorService(logger);
  const _doctorTokenService = new DoctorTokenService(logger);

  this.getAllDoctors = async (req, res, next) => {
    let doctors;
    try {
      doctors = await _doctorService.getAllDoctors();
    } catch (err) {
      const error = new BaseHttpError('Doctors retrieval error', 500);
      return next(error);
    }
    res.doctors = doctors;
    return next();
  };

  this.setIsAvailableFlag = async (req, res, next) => {
    const doctors = res.doctors;
    const currentTime = moment();
    const currentDay = currentTime.day(); // 0-6 Sunday-Saturday
    const currentHours = currentTime.hours(); // 0-24 hs format
    doctors.forEach((doctor) => {
      doctor.isAvailable = false;
      const businessHours = doctor.business_hours;
      businessHours.forEach((businessHour) => {
        if (currentDay == businessHour.day &&
          (currentHours >= businessHour.start_time) && (currentHours <= businessHour.end_time)) {
          doctor.isAvailable = true;
        }
      });
    });
    res.doctors = doctors;
    return next();
  };

  this.getAppointments = async (req, res, next) => {
    const doctorId = res.doctorAuthenticated.doctor_id;
    let appointments;
    try {
      appointments = await _doctorService.getAppointments(doctorId);
    } catch (err) {
      const error = new BaseHttpError('Appointments retrieval error', 500);
      return next(error);
    }
    res.appointments = appointments;
    return next();
  };

  this.generateToken = async (req, res, next) => {
    const doctor = res.doctor;
    let token;
    try {
      token = await _doctorTokenService.generateToken(doctor);
    } catch (err) {
      logger.error(`An error occurred while generating the token for doctor: ${doctor.doctor_id}`);
      return next(err);
    }
    res.token = token;
    return next();
  };
}

module.exports = DoctorController;
