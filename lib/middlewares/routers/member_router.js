const MemberTokenAuthenticator = require('../authenticators/member_token_authenticator.js');
const PasswordAuthenticator = require('../authenticators/password_authenticator.js');
const MemberController = require('../controllers/member_controller.js');
const AppointmentController = require('../controllers/appointment_controller.js');
const AppointmentResponseBuilder = require('../response_builders/appointment_response_builder.js');
const AppointmentRatingResponseBuilder = require('../response_builders/appointment_rating_response_builder.js');
const TokenResponseBuilder = require('../response_builders/token_response_builder.js');
const MedicalSpecialtiesResponseBuilder = require('../response_builders/medical_specialties_response_builder.js');
const MemberAppointmentsResponseBuilder = require('../response_builders/member_appointments_response_builder.js');
const MemberDeviceRegistrationResponseBuilder = require('../response_builders/member_device_registration_response_builder.js');

function MemberRouter(app, logger) {
  const _memberTokenAuthenticator = new MemberTokenAuthenticator(logger);
  const _passwordAuthenticator = new PasswordAuthenticator(logger);
  const _memberController = new MemberController(logger);
  const _appointmentController = new AppointmentController(logger);
  const _appointmentResponseBuilder = new AppointmentResponseBuilder(logger);
  const _appointmentRatingResponseBuilder = new AppointmentRatingResponseBuilder(logger);
  const _tokenResponseBuilder = new TokenResponseBuilder(logger);
  const _medicalSpecialtiesResponseBuilder = new MedicalSpecialtiesResponseBuilder(logger);
  const _memberAppointmentsResponseBuilder = new MemberAppointmentsResponseBuilder(logger);
  const _memberDeviceRegistrationResponseBuilder = new MemberDeviceRegistrationResponseBuilder(logger);

  app.post('/api/member/login',
      _passwordAuthenticator.authenticateMember,
      _memberController.generateToken,
      _tokenResponseBuilder.buildResponse,
  );

  app.get('/api/member/medical_specialties',
      _memberTokenAuthenticator.authenticate,
      _appointmentController.getMedicalSpecialties,
      _medicalSpecialtiesResponseBuilder.buildResponse,
  );

  app.post('/api/member/register_device',
    _memberTokenAuthenticator.authenticate,
    _memberController.registerDevice,
    _memberDeviceRegistrationResponseBuilder.buildResponse,
  );

  app.get('/api/member/appointment/:appointmentId',
      _memberTokenAuthenticator.authenticate,
      _appointmentController.getAppointment,
      _appointmentController.getMedicalSpecialtyForAppointment,
      _appointmentController.calculateWaitingMembers,
      _appointmentResponseBuilder.buildResponse,
  );

  app.get('/api/member/appointments',
      _memberTokenAuthenticator.authenticate,
      _memberController.getAppointments,
      _memberAppointmentsResponseBuilder.buildResponse,
  );

  app.post('/api/member/appointment',
      _memberTokenAuthenticator.authenticate,
      _appointmentController.requestAppointment,
      _appointmentController.getMedicalSpecialtyForAppointment,
      _appointmentController.generateQr,
      _appointmentController.calculateWaitingMembers,
      _appointmentResponseBuilder.buildResponse,
  );

  app.post('/api/member/appointment/:appointmentId/rating',
      _memberTokenAuthenticator.authenticate,
      _appointmentController.getAppointment,
      _appointmentController.rateAppointment,
      _appointmentRatingResponseBuilder.buildResponse,
  );

  app.post('/api/member/appointment/:appointmentId/start',
      _memberTokenAuthenticator.authenticate,
      _appointmentController.getAppointment,
      _appointmentController.updateAppointmentOnMemberConnection,
      _appointmentResponseBuilder.buildResponse,
  );

  app.post('/api/member/appointment/:appointmentId/finish',
      _memberTokenAuthenticator.authenticate,
      _appointmentController.getAppointment,
      _appointmentController.updateAppointmentOnMemberDisconnection,
      _appointmentResponseBuilder.buildResponse,
  );
}

module.exports = MemberRouter;
