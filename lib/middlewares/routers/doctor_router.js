const PasswordAuthenticator = require('../authenticators/password_authenticator.js');
const DoctorTokenAuthenticator = require('../authenticators/doctor_token_authenticator.js');
const DoctorController = require('../controllers/doctor_controller.js');
const MemberController = require('../controllers/member_controller.js');
const AppointmentController = require('../controllers/appointment_controller.js');
const DoctorsResponseBuilder = require('../response_builders/doctors_response_builder.js');
const AppointmentResponseBuilder = require('../response_builders/appointment_response_builder.js');
const DoctorAppointmentsResponseBuilder = require('../response_builders/doctor_appointments_response_builder.js');
const QueuedAppointmentsResponseBuilder = require('../response_builders/queued_appointments_response_builder.js');
const TokenResponseBuilder = require('../response_builders/token_response_builder.js');

function DoctorRouter(app, logger) {
  const _doctorTokenAuthenticator = new DoctorTokenAuthenticator(logger);
  const _passwordAuthenticator = new PasswordAuthenticator(logger);
  const _doctorController = new DoctorController(logger);
  const _memberController = new MemberController(logger);
  const _appointmentController = new AppointmentController(logger);
  const _doctorsResponseBuilder = new DoctorsResponseBuilder(logger);
  const _appointmentResponseBuilder = new AppointmentResponseBuilder(logger);
  const _doctorAppointmentsResponseBuilder = new DoctorAppointmentsResponseBuilder(logger);
  const _queuedAppointmentsResponseBuilder = new QueuedAppointmentsResponseBuilder(logger);
  const _tokenResponseBuilder = new TokenResponseBuilder(logger);

  app.post('/api/doctor/login',
      _passwordAuthenticator.authenticateDoctor,
      _doctorController.generateToken,
      _tokenResponseBuilder.buildResponse,
  );

  app.get('/api/doctors',
      _doctorController.getAllDoctors,
      _doctorController.setIsAvailableFlag,
      _doctorsResponseBuilder.buildResponse,
  );

  app.get('/api/doctor/appointments',
      _doctorTokenAuthenticator.authenticate,
      _doctorController.getAppointments,
      _doctorAppointmentsResponseBuilder.buildResponse,
  );

  app.get('/api/doctor/queued_appointments',
      _doctorTokenAuthenticator.authenticate,
      _appointmentController.getQueuedAppointments,
      _appointmentController.calculateMemberWaitTime,
      _queuedAppointmentsResponseBuilder.buildResponse,
  );

  app.post('/api/doctor/appointment/:appointmentId/data',
    _doctorTokenAuthenticator.authenticate,
    _appointmentController.getAppointment,
    _appointmentController.checkAppointmentOwnership,
    _appointmentController.saveAppointmentData,
    _appointmentResponseBuilder.buildResponse,
  );

  app.post('/api/doctor/appointment/:appointmentId/start',
      _doctorTokenAuthenticator.authenticate,
      _appointmentController.getAppointment,
      _appointmentController.assignAppointmentToDoctor,
      _appointmentController.updateAppointmentOnDoctorConnection,
      _memberController.sendNotificationsOnAppointmentStart,
      _appointmentResponseBuilder.buildResponse,
  );

  app.post('/api/doctor/appointment/:appointmentId/finish',
      _doctorTokenAuthenticator.authenticate,
      _appointmentController.getAppointment,
      _appointmentController.updateAppointmentOnDoctorDisconnection,
      _appointmentResponseBuilder.buildResponse,
  );
}

module.exports = DoctorRouter;
