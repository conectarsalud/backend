const MemberService = require('../../services/member_service.js');
const MemberTokenService = require('../../services/member_token_service.js');
const BaseHttpError = require('../../errors/base_http_error.js');

function MemberTokenAuthenticator(logger) {
  const _memberService = new MemberService(logger);
  const _memberTokenService = new MemberTokenService(logger);

  function getTokenFromHeader(req) {
    if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
      return req.headers.authorization.split(' ')[1];
    } else return null;
  }

  async function authenticate(res, next, token) {
    if (token) {
      const memberId = await _memberTokenService.validateToken(token);
      if (memberId) {
        res.memberAuthenticated = await _memberService.getMember(memberId);
        return next();
      }
    }
    const error = new BaseHttpError('Unauthorized', 401);
    return next(error);
  }

  this.authenticate = async (req, res, next) => {
    const token = getTokenFromHeader(req);
    return await authenticate(res, next, token);
  };
}

module.exports = MemberTokenAuthenticator;
