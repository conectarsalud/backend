const MemberService = require('../../services/member_service.js');
const DoctorService = require('../../services/doctor_service.js');
const BaseHttpError = require('../../errors/base_http_error.js');

function PasswordAuthenticator(logger) {
  const _memberService = new MemberService(logger);
  const _doctorService = new DoctorService(logger);

  this.authenticateMember = async (req, res, next) => {
    let member;
    try {
      member = await _memberService.authenticateWithPassword(req.body.username, req.body.password);
    } catch (err) {
      const error = new BaseHttpError('Wrong username or password.', 401);
      return next(error);
    }
    res.member = member;
    return next();
  };

  this.authenticateDoctor = async (req, res, next) => {
    let doctor;
    try {
      doctor = await _doctorService.authenticateWithPassword(req.body.username, req.body.password);
    } catch (err) {
      const error = new BaseHttpError('Wrong username or password.', 401);
      return next(error);
    }
    res.doctor = doctor;
    return next();
  };
}

module.exports = PasswordAuthenticator;
