const DoctorService = require('../../services/doctor_service.js');
const DoctorTokenService = require('../../services/doctor_token_service.js');
const BaseHttpError = require('../../errors/base_http_error.js');

function DoctorTokenAuthenticator(logger) {
  const _doctorService = new DoctorService(logger);
  const _doctorTokenService = new DoctorTokenService(logger);

  function getTokenFromHeader(req) {
    if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
      return req.headers.authorization.split(' ')[1];
    } else return null;
  }

  async function authenticate(res, next, token) {
    if (token) {
      const doctorId = await _doctorTokenService.validateToken(token);
      if (doctorId) {
        res.doctorAuthenticated = await _doctorService.getDoctor(doctorId);
        return next();
      }
    }
    const error = new BaseHttpError('Unauthorized', 401);
    return next(error);
  }

  this.authenticate = async (req, res, next) => {
    const token = getTokenFromHeader(req);
    return await authenticate(res, next, token);
  };
}

module.exports = DoctorTokenAuthenticator;
