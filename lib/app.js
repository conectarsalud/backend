require('dotenv').config();
const config = require('config');
const cors = require('cors');
const express = require('express');
const Logger = require('./utils/logger.js');
const memberRouter = require('./middlewares/routers/member_router.js');
const doctorRouter = require('./middlewares/routers/doctor_router.js');
const errorMiddleware = require('./middlewares/main_error_handler.js');

const logger = new Logger();

const app = express();

app.use(express.json({ limit: '20mb' }));

// Accepts requests from all the origins
app.use(cors());

// Add routers to the API
memberRouter(app, logger);
doctorRouter(app, logger);

// Add basic error middleware to handle all errors
errorMiddleware(app, logger);

// Start the app in the designated port and host
if (! module.parent) {
  app.listen(config.EXPRESS.PORT, config.EXPRESS.HOST, () => {
    logger.info(`Server running on http://${config.EXPRESS.HOST}:${config.EXPRESS.PORT}`);
  });
}

module.exports = app;
