const crypto = require('crypto');
const DoctorModel = require('../models/doctor_model.js');

class DoctorService {
  constructor(logger) {
    this.logger = logger;
    this.doctorModel = new DoctorModel(logger);
  }

  hash(s) {
    return crypto.createHmac('sha256', 'secret').update(s).digest('hex');
  }

  async authenticateWithPassword(username, password) {
    const doctor = await this.doctorModel.getByUsername(username);
    if (doctor) {
      if (this.hash(password) == doctor.password) {
        this.logger.info('Password validated successfully for username: %s', username);
        return doctor;
      } else {
        this.logger.error('Wrong password for username: %s', username);
        throw new Error('Password incorrect');
      }
    } else throw new Error('Member not found');
  };

  async getAllDoctors() {
    let doctors;
    try {
      doctors = await this.doctorModel.getAllDoctors();
    } catch (findErr) {
      this.logger.error('An error occurred while retrieving all the doctors');
      throw findErr;
    }
    return doctors;
  };

  async getDoctor(doctorId) {
    let doctor;
    try {
      doctor = await this.doctorModel.getDoctorById(doctorId);
    } catch (findErr) {
      this.logger.error(`An error occurred while retrieving the doctor ${doctorId}`);
      throw findErr;
    }
    return doctor;
  };

  async getDoctorSpecialties(doctorId) {
    let doctorSpecialties;
    try {
      doctorSpecialties = await this.doctorModel.getDoctorSpecialties(doctorId);
    } catch (findErr) {
      this.logger.error(`An error occurred while retrieving the specialties for doctor ${doctorId}`);
      throw findErr;
    }
    return doctorSpecialties;
  };

  async getAppointments(doctorId) {
    let doctorAppointments;
    try {
      doctorAppointments = await this.doctorModel.getDoctorAppointments(doctorId);
    } catch (findErr) {
      this.logger.error(`An error occurred while retrieving the appointments for doctor ${doctorId}`);
      throw findErr;
    }
    return doctorAppointments;
  };
}

module.exports = DoctorService;
