const TokenGenerationService = require('./token_generation_service.js');

class DoctorTokenGenerationService {
  constructor(logger) {
    this.logger = logger;
    this.tokenGenerationService = new TokenGenerationService(logger);
  }

  getDoctorData(doctor) {
    return {
      id: doctor.doctor_id,
    };
  }

  async generateToken(doctor) {
    const doctorData = this.getDoctorData(doctor);
    const doctorExpiration = '12h';
    let token;
    try {
      token = await this.tokenGenerationService.generateToken(doctorData, doctorExpiration);
    } catch (err) {
      this.logger.error(`Error creating token for doctor: '${doctor.doctor_id}'`);
      throw err;
    }
    this.logger.info(`Token created for doctor: '${doctor.doctor_id}'`);
    return token;
  };

  isValidOwner(decodedData, owner) {
    return decodedData && (decodedData.id == owner.id) && (decodedData.name == owner.name);
  }

  async validateToken(token, doctor) {
    let validatedToken;
    try {
      validatedToken = await this.tokenGenerationService.validateToken(token, (decodedData) => {
        return this.isValidOwner(decodedData, this.getDoctorData(doctor));
      });
    } catch (err) {
      this.logger.error(`Error validating token for doctor: '${doctor.doctor_id}'`);
      throw err;
    }
    this.logger.info(`Token validated for doctor: '${doctor.doctor_id}'`);
    return validatedToken;
  };

  async getDoctorIdFromToken(token) {
    const decodedTokenData = await this.tokenGenerationService.decodeTokenData(token);
    return decodedTokenData.id;
  };
}

module.exports = DoctorTokenGenerationService;
