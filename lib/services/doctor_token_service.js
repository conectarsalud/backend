const DoctorTokenGenerationService = require('./doctor_token_generation_service.js');
const DoctorTokenModel = require('../models/doctor_token_model.js');

class DoctorTokenService {
  constructor(logger) {
    this.logger = logger;
    this.doctorTokenGenerationService = new DoctorTokenGenerationService(logger);
    this.doctorTokenModel = new DoctorTokenModel(logger);
  }

  async generateNewTokenForDoctor(doctor) {
    const token = await this.doctorTokenGenerationService.generateToken(doctor);
    await this.doctorTokenModel.upsert(doctor, token);
    const doctorToken = {
      token: token.token,
      tokenExpiration: token.expiresAt,
    };
    return doctorToken;
  }

  async generateToken(doctor) {
    const doctorToken = await this.doctorTokenModel.getByDoctorId(doctor.doctor_id);
    if (doctorToken) {
      try {
        const validatedToken = await this.doctorTokenGenerationService.validateToken(doctorToken.token, doctor);
        this.logger.info(`Doctor: '${doctor.doctor_id}' already has a valid token, skipping token generation`);
        const businessToken = {
          token: validatedToken.token,
          tokenExpiration: validatedToken.expiresAt,
        };
        return businessToken;
      } catch (err) {
        this.logger.info(`Doctor: '${doctor.doctor_id}' already has a token but it is not valid, generating new token`);
        return await this.generateNewTokenForDoctor(doctor);
      }
    } else {
      this.logger.info(`Doctor: '${doctor.doctor_id}' does not have a token, generating one`);
      return await this.generateNewTokenForDoctor(doctor);
    }
  };

  async validateToken(token) {
    let doctorId;
    try {
      doctorId = await this.doctorTokenGenerationService.getDoctorIdFromToken(token);
    } catch (err) {
      this.logger.error(`Error getting doctor id from token ${token}`);
      return null;
    }
    const doctorToken = await this.doctorTokenModel.getByDoctorId(doctorId);
    if ((doctorToken) && (token === doctorToken.token)) {
      this.logger.info(`Token was validated successfully for doctor_id: '${doctorId}'`);
      return doctorId;
    } else {
      this.logger.debug(`Token was created for doctor_id: '${doctorId}' but does not match the token saved in the database for that doctor`);
      this.logger.error('Token contains inconsistent data');
      return null;
    }
  };
}

module.exports = DoctorTokenService;
