const qrCode = require('qrcode');
const path = require('path');
const { v4: uuidv4 } = require('uuid');
const fs = require('fs-extra');
const GoogleUploadService = require('./google_upload_service.js');

class QrGeneratorService {
  constructor(logger) {
    this.logger = logger;
    this.googleUpdateService = new GoogleUploadService(logger);
  }

  async createAndUploadQrImage(qrContent) {
    const tempFilename = uuidv4() + '.png';
    const tempFilesFolder = 'tempImages';
    await fs.ensureDir(tempFilesFolder);
    const tempFilepath = path.join(tempFilesFolder, tempFilename);
    const imageOptions = {
      margin: 2,
    };
    await qrCode.toFile(tempFilepath, JSON.stringify(qrContent), imageOptions);
    const fileUploaded = await this.googleUpdateService.uploadFromLocal(tempFilepath);
    return fileUploaded.resource;
  }

  async generateQrAndUpload(qrContent) {
    const qrImageUrl = await this.createAndUploadQrImage(qrContent);
    return qrImageUrl;
  }
}

module.exports = QrGeneratorService;
