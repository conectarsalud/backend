const jwt = require('jsonwebtoken');

class TokenGenerationService {
  constructor(logger) {
    this.logger = logger;
  }

  createTokenObject(token) {
    const tokenData = jwt.decode(token);
    return { token: token, expiresAt: tokenData.exp };
  }

  async generateToken(data, expiration) {
    // replace hardcoded secret with private key
    let token;
    try {
      if (expiration) {
        token = await jwt.sign({ data: data }, 'secret', { expiresIn: expiration });
      } else {
        token = await jwt.sign({ data: data }, 'secret');
      }
    } catch (err) {
      this.logger.error('Token generation failed');
      throw err;
    }
    this.logger.info('Token created successfully');
    return this.createTokenObject(token);
  }

  async decodeTokenData(token) {
    const decoded = await this.verifyTokenAndDecode(token);
    return decoded.data;
  }

  async validateToken(token, validateFunction) {
    const decoded = await this.verifyTokenAndDecode(token);
    if (validateFunction(decoded.data)) {
      this.logger.info(`Token ${token} was validated successfully`);
      return { token: token, expiresAt: decoded.exp };
    } else {
      this.logger.error(`Token ${token} could not be validated`);
      throw new Error('Token validation failed');
    }
  }
  async verifyTokenAndDecode(token) {
    // replace hardcoded secret with private key
    try {
      const decoded = await jwt.verify(token, 'secret');
      return decoded;
    } catch (err) {
      this.logger.error(`Token ${token} could not be validated due to a failure: ${err.message}`);
      throw err;
    }
  }
}

module.exports = TokenGenerationService;
