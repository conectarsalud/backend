const crypto = require('crypto');
const MemberModel = require('../models/member_model.js');

class MemberService {
  constructor(logger) {
    this.logger = logger;
    this.memberModel = new MemberModel(logger);
  }

  hash(s) {
    return crypto.createHmac('sha256', 'secret').update(s).digest('hex');
  }

  async authenticateWithPassword(username, password) {
    const member = await this.memberModel.getByUsername(username);
    if (member) {
      if (this.hash(password) == member.password) {
        this.logger.info('Password validated successfully for username: %s', username);
        return member;
      } else {
        this.logger.error('Wrong password for username: %s', username);
        throw new Error('Password incorrect');
      }
    } else throw new Error('Member not found');
  };

  async getMember(memberId) {
    let member;
    try {
      member = await this.memberModel.getMemberById(memberId);
    } catch (findErr) {
      this.logger.error(`An error occurred while retrieving the member ${memberId}`);
      throw findErr;
    }
    return member;
  }

  async getAppointments(memberId) {
    let memberAppointments;
    try {
      memberAppointments = await this.memberModel.getMemberAppointments(memberId);
    } catch (findErr) {
      this.logger.error(`An error occurred while retrieving the appointments for member ${memberId}`);
      throw findErr;
    }
    return memberAppointments;
  };
}

module.exports = MemberService;
