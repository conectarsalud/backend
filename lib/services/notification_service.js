const admin = require('firebase-admin');
const MemberDeviceTokenService = require('./member_device_token_service.js');

const serviceAccount = require('../../conectarsalud-277922-firebase-adminsdk-l9wgo-a51a901170.json');
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  //databaseURL: "https://conectarsalud-277922.firebaseio.com"
});

class NotificationService {

  constructor(logger) {
    this.logger = logger;
    this.memberDeviceTokenService = new MemberDeviceTokenService(logger);
  }

  async sendNotificationToMember(memberId, notificationMessage, appointmentId){
    this.logger.info(`Sending notification to member_id: ${memberId}`);
    let memberDeviceToken = await this.memberDeviceTokenService.getDevice(memberId);
    if (! memberDeviceToken) {
      this.logger.error(`No device found for member_id: ${memberId}`);
      return;
    }
    const message = {
      token: memberDeviceToken.device_token,
      notification: {
        title: "Conectar Salud - Notificación",
        body: notificationMessage,
      },
      data: {
        appointment_id: appointmentId.toString(),
      },
    }

    // Send a message to the device corresponding to the provided
    // registration token.
    let response;
    try{
      response = await admin.messaging().send(message);
      // Response is a message ID string.
      this.logger.info(`Notification successfully sent message to member_id: ${memberId}`);
      this.logger.debug(`Notification response: %j`, response);
    } catch (err){
      this.logger.error('Error sending message: %s', err.errorInfo.message);
    }
  }

}

module.exports = NotificationService;
