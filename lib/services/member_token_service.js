const MemberTokenGenerationService = require('./member_token_generation_service.js');
const MemberTokenModel = require('../models/member_token_model.js');

class MemberTokenService {
  constructor(logger) {
    this.logger = logger;
    this.memberTokenGenerationService = new MemberTokenGenerationService(logger);
    this.memberTokenModel = new MemberTokenModel(logger);
  }

  async generateNewTokenForMember(member) {
    const token = await this.memberTokenGenerationService.generateToken(member);
    await this.memberTokenModel.upsert(member, token);
    const memberToken = {
      token: token.token,
      tokenExpiration: token.expiresAt,
    };
    return memberToken;
  }

  async generateToken(member) {
    const memberToken = await this.memberTokenModel.getByMemberId(member.member_id);
    if (memberToken) {
      try {
        const validatedToken = await this.memberTokenGenerationService.validateToken(memberToken.token, member);
        this.logger.info(`Member: '${member.member_id}' already has a valid token, skipping token generation`);
        const businessToken = {
          token: validatedToken.token,
          tokenExpiration: validatedToken.expiresAt,
        };
        return businessToken;
      } catch (err) {
        this.logger.info(`Member: '${member.member_id}' already has a token but it is not valid, generating new token`);
        return await this.generateNewTokenForMember(member);
      }
    } else {
      this.logger.info(`Member: '${member.member_id}' does not have a token, generating one`);
      return await this.generateNewTokenForMember(member);
    }
  };

  async validateToken(token) {
    let memberId;
    try {
      memberId = await this.memberTokenGenerationService.getMemberIdFromToken(token);
    } catch (err) {
      this.logger.error(`Error getting member id from token ${token}`);
      return null;
    }
    const memberToken = await this.memberTokenModel.getByMemberId(memberId);
    if ((memberToken) && (token === memberToken.token)) {
      this.logger.info(`Token was validated successfully for member_id: '${memberId}'`);
      return memberId;
    } else {
      this.logger.debug(`Token was created for member_id: '${memberId}' but does not match the token saved in the database for that member`);
      this.logger.error('Token contains inconsistent data');
      return null;
    }
  };
}

module.exports = MemberTokenService;
