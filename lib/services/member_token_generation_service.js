const TokenGenerationService = require('./token_generation_service.js');

class MemberTokenGenerationService {
  constructor(logger) {
    this.logger = logger;
    this.tokenGenerationService = new TokenGenerationService(logger);
  }

  getMemberData(member) {
    return {
      id: member.member_id,
    };
  }

  async generateToken(member) {
    const memberData = this.getMemberData(member);
    const memberExpiration = '12h';
    let token;
    try {
      token = await this.tokenGenerationService.generateToken(memberData, memberExpiration);
    } catch (err) {
      this.logger.error(`Error creating token for member: '${member.member_id}'`);
      throw err;
    }
    this.logger.info(`Token created for member: '${member.member_id}'`);
    return token;
  };

  isValidOwner(decodedData, owner) {
    return decodedData && (decodedData.id == owner.id) && (decodedData.name == owner.name);
  }

  async validateToken(token, member) {
    let validatedToken;
    try {
      validatedToken = await this.tokenGenerationService.validateToken(token, (decodedData) => {
        return this.isValidOwner(decodedData, this.getMemberData(member));
      });
    } catch (err) {
      this.logger.error(`Error validating token for member: '${member.member_id}'`);
      throw err;
    }
    this.logger.info(`Token validated for member: '${member.member_id}'`);
    return validatedToken;
  };

  async getMemberIdFromToken(token) {
    const decodedTokenData = await this.tokenGenerationService.decodeTokenData(token);
    return decodedTokenData.id;
  };
}

module.exports = MemberTokenGenerationService;
