const MemberDeviceTokenModel = require('../models/member_device_token_model.js');

class MemberDeviceTokenService {
  constructor(logger) {
    this.logger = logger;
    this.memberDeviceTokenModel = new MemberDeviceTokenModel(logger);
  }

  async registerDevice(memberId, token) {
    let memberDeviceToken;
    try {
      memberDeviceToken = await this.memberDeviceTokenModel.upsert(memberId, token);
    } catch(err){
      this.logger.error(`An error ocurred while registering the device token for member ${memberId}`);
      throw err;
    }
    return memberDeviceToken;
  }

  async getDevice(memberId){
    let memberDeviceToken;
    try {
      memberDeviceToken = await this.memberDeviceTokenModel.getByMemberId(memberId);
    } catch(err){
      this.logger.error(`An error ocurred while retrieving the device token for member ${memberId}`);
      throw err;
    }
    return memberDeviceToken;
  }
}

module.exports = MemberDeviceTokenService;
