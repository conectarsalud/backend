const config = require('config');
const path = require('path');
const { Storage } = require('@google-cloud/storage');

const projectId = config.GOOGLE_CLOUD_PROJECT_ID;
const bucketName = config.GOOGLE_CLOUD_BUCKET_NAME;

const storage = new Storage({
  projectId: projectId,
  keyFilename: 'ConectarSalud-58d0adc49c75.json',
});

function GoogleUploadService(logger) {
  const _logger = logger;

  function getPublicUrl(filePath) {
    return `https://storage.googleapis.com/${bucketName}/${filePath}`;
  }

  function getFileModel(filePath) {
    const fileModel = {};
    fileModel.resource = encodeURI(getPublicUrl(filePath));
    fileModel.file_name = path.basename(filePath);
    return fileModel;
  }

  function getStorageOptions(gcsFilename) {
    return {
      public: true,
      destination: gcsFilename,
    };
  }

  this.uploadFromLocal = async (localFilepath) => {
    const bucket = storage.bucket(bucketName);
    const filename = localFilepath.substring(localFilepath.indexOf(path.sep) + 1); // remove root folder from path
    const storageOptions = getStorageOptions(filename);
    try {
      await bucket.upload(localFilepath, storageOptions);
    } catch (err) {
      _logger.error('Error uploading file with key: \'%s\' to bucket: \'%s\'', filename, bucketName);
      throw err;
    }
    _logger.info('File with key: \'%s\' was uploaded successfully to bucket: \'%s\'', filename, bucketName);
    return getFileModel(filename);
  };

  this.updateRemoteFilename = async (oldFilename, newFilename, serverOwner) => {
    const oldFilepath = path.join(serverOwner, oldFilename);
    const newFilepath = path.join(serverOwner, newFilename);
    const bucket = storage.bucket(bucketName);
    try {
      const file = await bucket.file(oldFilepath);
      await file.move(newFilepath);
    } catch (err) {
      _logger.error('Error renaming file with key: \'%s\' to new key :\'%s\' in bucket: \'%s\'', oldFilename, newFilename, bucketName);
      _logger.debug('Remote file update error: %j', err);
      throw err;
    }
    _logger.info('File with key: \'%s\' was successfully renamed to: \'%s\' in bucket: \'%s\'', oldFilename, newFilename, bucketName);
    return getFileModel(newFilepath);
  };

  this.deleteRemoteFile = async (filename, serverOwner) => {
    const filepath = path.join(serverOwner, filename);
    const bucket = storage.bucket(bucketName);
    try {
      const file = await bucket.file(filepath);
      await file.delete();
    } catch (err) {
      _logger.error('Error deleting file with key: \'%s\' in bucket: \'%s\'', filepath, bucketName);
      throw err;
    }
    _logger.info('File with key: \'%s\' was deleted successfully in bucket: \'%s\'', filepath, bucketName);
    return;
  };

  this.deleteBucketContent = async () => {
    const bucket = storage.bucket(bucketName);
    try {
      const results = await bucket.getFiles();
      const files = results[0];
      await Promise.all(files.map(async (file) => {
        await file.delete();
        _logger.info('File with key: \'%s\' was deleted successfully in bucket: \'%s\'', file.name, bucketName);
      }));
    } catch (err) {
      _logger.error('Error deleting file with key: \'%s\' in bucket: \'%s\'', file.name, bucketName);
      throw err;
    }
    return;
  };
}

module.exports = GoogleUploadService;
