const { v4: uuidv4 } = require('uuid');
const { APPOINTMENT_STATES } = require('../../lib/utils/constants.js');
const AppointmentModel = require('../models/appointment_model.js');
const AppointmentDataModel = require('../models/appointment_data_model.js');
const AppointmentRatingModel = require('../models/appointment_rating_model.js');

class AppointmentService {
  constructor(logger) {
    this.logger = logger;
    this.appointmentModel = new AppointmentModel(logger);
    this.appointmentDataModel = new AppointmentDataModel(logger);
    this.appointmentRatingModel = new AppointmentRatingModel(logger);
  }

  async getMedicalSpecialties() {
    let medicalSpecialties;
    try {
      medicalSpecialties = await this.appointmentModel.getMedicalSpecialties();
    } catch (findErr) {
      this.logger.error('An error occurred while retrieving all the medical specialties');
      throw findErr;
    }
    return medicalSpecialties;
  };

  async getMedicalSpecialty(medicalSpecialtyId) {
    let medicalSpecialty;
    try {
      medicalSpecialty = await this.appointmentModel.getMedicalSpecialty(medicalSpecialtyId);
    } catch (findErr) {
      this.logger.error(`An error occurred while retrieving the medical specialty ${medicalSpecialtyId}`);
      throw findErr;
    }
    return medicalSpecialty;
  };

  async newAppointment(memberId, medicalSpecialty, reason) {
    let appointment;
    const channelId = uuidv4();
    const initialStatus = APPOINTMENT_STATES.QUEUED;
    try {
      appointment = await this.appointmentModel.createNew(memberId, initialStatus, channelId, medicalSpecialty, reason);
    } catch (createErr) {
      this.logger.error(`An error occurred while creating the appointment for ${memberId}`);
      throw createErr;
    }
    return appointment;
  }

  async retrieveAppointment(appointmentId) {
    let appointment;
    try {
      appointment = await this.appointmentModel.getAppointmentById(appointmentId);
    } catch (findErr) {
      this.logger.error(`An error occurred while retrieving the appointment ${appointmentId}`);
      throw findErr;
    }
    return appointment;
  }

  async updateAppointment(appointment) {
    try {
      await this.appointmentModel.update(appointment);
    } catch (updateErr) {
      this.logger.error(`An error occurred while updating the appointment ${appointment.appointment_id}`);
    }
  }

  async rateAppointment(appointmentId, rating, comments) {
    let appointmentRating;
    try {
      appointmentRating = await this.appointmentRatingModel.createNew(appointmentId, rating, comments);
    } catch (createErr) {
      this.logger.error(`An error occurred while saving the rating for appointment ${appointmentId}`);
      throw createErr;
    }
    return appointmentRating;
  }

  async getQueuedAppointments() {
    let queuedAppointments;
    try {
      queuedAppointments = await this.appointmentModel.getQueuedAppointments();
    } catch (findErr) {
      this.logger.error(`An error occurred while retrieving the queued appointments`);
      throw findErr;
    }
    return queuedAppointments;
  };

  async getWaitingMembersCount(appointment) {
    let waitingMembersCount;
    try {
      waitingMembersCount = await this.appointmentModel.getWaitingMembersCount(appointment);
    } catch (findErr) {
      this.logger.error(`An error occurred while retrieving the waiting members count`);
      throw findErr;
    }
    return waitingMembersCount;
  };

  async saveAppointmentData(appointmentId, indications){
    let appointmentDataModel;
    try {
      appointmentDataModel = await this.appointmentDataModel.upsert(appointmentId, indications);
    } catch(err){
      this.logger.error(`An error ocurred while saving the data for appointment ${appointmentId}`);
      throw err;
    }
    return appointmentDataModel;
  }
}

module.exports = AppointmentService;
