module.exports = {
  APPOINTMENT_STATES: {
    QUEUED: 'Queued',
    ASSIGNED: 'Assigned',
    WAITING_MEMBER: 'Waiting member',
    WAITING_DOCTOR: 'Waiting doctor',
    IN_PROGRESS: 'In progress',
    MEMBER_DISCONNECT: 'Member disconnect',
    DOCTOR_DISCONNECT: 'Doctor disconnect',
    FINISHED: 'Finished',
  },
  MEMBER_NOTIFICATIONS_MESSAGES: {
    FIVE_IN_QUEUE: 'Sólo quedan 5 personas delante tuyo, muy pronto serás atendido.',
    NEXT_IN_QUEUE: 'Sos el próximo en la lista de espera, quedate cerca del teléfono.',
    APPOINTMENT_START: 'El profesional ya está esperándote. Toca aquí para iniciar la videollamada.',
  },
};
