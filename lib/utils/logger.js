const winston = require('winston');
const logFilename = 'all-logs.log';

function Logger() {
  const level = process.env.LOG_LEVEL || 'debug';

  const logger = winston.createLogger({
    transports: [
      new winston.transports.File({
        level: level,
        filename: logFilename,
        format: winston.format.combine(
            winston.format.splat(),
            winston.format.simple(),
        ),
      }),
      new winston.transports.Console({
        level: level,
        format: winston.format.combine(
            winston.format.splat(),
            winston.format.colorize(),
            winston.format.simple(),
        ),
      }),
    ],
  });

  this.error = function(...args) {
    logger.error(...args);
  };

  this.info = function(...args) {
    logger.info(...args);
  };

  this.debug = function(...args) {
    logger.debug(...args);
  };

  this.warn = function(...args) {
    logger.warn(...args);
  };
}

module.exports = Logger;
