const moment = require('moment-timezone');
const DEFAULT_TIMEZONE = 'America/Argentina/Buenos_Aires';
const DEFAULT_DATE_FORMAT = 'YYYY-MM-DD HH:mm:ss';

function convertFromUtcToLocal(utcDate) {
  return moment(utcDate).tz(DEFAULT_TIMEZONE).format(DEFAULT_DATE_FORMAT);
}

function getCurrentLocalTime() {
  return moment().tz(DEFAULT_TIMEZONE).format(DEFAULT_DATE_FORMAT);
}

module.exports = { convertFromUtcToLocal, getCurrentLocalTime };
