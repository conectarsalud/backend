exports.EXPRESS = {
  HOST: process.env.HOST || '0.0.0.0',
  PORT: process.env.PORT || 8080,
};

exports.DATABASE_URL = 'postgres://curvzjcayxvuhv:93f7004475beb8262f2eaab29f4599f7238095a246a74f744f061643a3cb4e67'+
  '@ec2-18-215-99-63.compute-1.amazonaws.com:5432/dqhns11bto2hg';

exports.GOOGLE_CLOUD_PROJECT_ID = 'conectarsalud-277922';
exports.GOOGLE_CLOUD_BUCKET_NAME = 'conectar-salud-main-bucket';

exports.SEND_MEMBER_NOTIFICATIONS = false;

