const pg = require('pg');
pg.defaults.ssl = {
  rejectUnauthorized: false,
};

module.exports = {
  client: 'postgresql',
  connection: process.env.DATABASE_URL || 'postgres://curvzjcayxvuhv:93f7004475beb8262f2eaab29f4599f7238095a246a74f744f061643a3cb4e67' +
      '@ec2-18-215-99-63.compute-1.amazonaws.com:5432/dqhns11bto2hg',
  pool: {
    min: 2,
    max: 10,
  },
  migrations: {
    tableName: 'knex_migrations',
    directory: './database/migrations',
  },
  seeds: {
    directory: './database/seeds',
  },
  onUpdateTrigger: (table) => `
    CREATE TRIGGER ${table}_updated_at
    BEFORE UPDATE ON ${table}
    FOR EACH ROW
    EXECUTE PROCEDURE on_update_timestamp();
  `,
};
