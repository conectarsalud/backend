const expect = require('chai').expect;
const sinon = require('sinon');
const proxyquire = require('proxyquire');
const supertest = require('supertest');
const moment = require('moment');
const { v4: uuidv4 } = require('uuid');
const database = require('../database/database.js');
const { getCurrentLocalTime } = require('../lib/utils/date_converter.js');
const app = require('../lib/app.js');
const loggerModuleFromApp = './utils/logger.js';
const appModule = '../lib/app.js';
const outputConsole = false;

const DOCTORS_DB_SIZE = 50;
const MEDICAL_SPECIALTIES_SIZE = 31;

describe('Integration Tests', function() {
  let request;

  before(()=>{
    if (outputConsole) {
      request = supertest(app);
    } else {
      const mockLogger = {
        debug: sinon.stub(),
        error: sinon.stub(),
        warn: sinon.stub(),
        info: sinon.stub(),
      };
      const mocks = {};
      mocks[loggerModuleFromApp] = function() {
        return mockLogger;
      };
      const mockApp = proxyquire(appModule, mocks);
      request = supertest(mockApp);
    }
  });

  after(()=>{
    database.destroyConnection();
  });

  describe('Member login', ()=>{
    describe('Success', ()=>{
      const validMember = {
        username: '1111',
        password: '37243062',
      };

      it('returns 200 when username and password are correct', async ()=>{
        await request
            .post('/api/member/login')
            .send(validMember)
            .expect(200);
      });

      it('returns expected response when username and password are correct', async ()=>{
        const response = await request
            .post('/api/member/login')
            .send(validMember);
        expect(response.body.token).to.be.ok;
        expect(response.body.token.token).to.be.ok;
        expect(response.body.token.tokenExpiration).to.be.ok;
      });
    });

    describe('Error', ()=>{
      const memberUnexistent = {
        username: 'notfound',
        password: 'notfound',
      };

      const memberWrongPassword = {
        username: '1111',
        password: '1111',
      };

      it('returns 401 when username is not found', async ()=>{
        await request
            .post('/api/member/login')
            .send(memberUnexistent)
            .expect(401);
      });

      it('returns expected response when username is not found', async ()=>{
        const response = await request
            .post('/api/member/login')
            .send(memberUnexistent);
        expect(response.body.code).to.equal(401);
        expect(response.body.message).to.equal(`Wrong username or password.`);
      });

      it('returns 401 when password is wrong', async ()=>{
        await request
            .post('/api/member/login')
            .send(memberWrongPassword)
            .expect(401);
      });

      it('returns expected response when password is wrong', async ()=>{
        const response = await request
            .post('/api/member/login')
            .send(memberWrongPassword);
        expect(response.body.code).to.equal(401);
        expect(response.body.message).to.equal(`Wrong username or password.`);
      });
    });
  });

  describe('Doctor login', ()=>{
    describe('Success', ()=>{
      const validDoctor = {
        username: '1111',
        password: '1111',
      };

      it('returns 200 when username and password are correct', async ()=>{
        await request
            .post('/api/doctor/login')
            .send(validDoctor)
            .expect(200);
      });

      it('returns expected response when username and password are correct', async ()=>{
        const response = await request
            .post('/api/doctor/login')
            .send(validDoctor);
        expect(response.body.token).to.be.ok;
        expect(response.body.token.token).to.be.ok;
        expect(response.body.token.tokenExpiration).to.be.ok;
      });
    });

    describe('Error', ()=>{
      const doctorUnexistent = {
        username: 'notfound',
        password: 'notfound',
      };

      const doctorWrongPassword = {
        username: '1111',
        password: '2222',
      };

      it('returns 401 when user is not found', async ()=>{
        await request
            .post('/api/doctor/login')
            .send(doctorUnexistent)
            .expect(401);
      });

      it('returns expected response when user is not found', async ()=>{
        const response = await request
            .post('/api/doctor/login')
            .send(doctorUnexistent);
        expect(response.body.code).to.equal(401);
        expect(response.body.message).to.equal(`Wrong username or password.`);
      });

      it('returns 401 when password is wrong', async ()=>{
        await request
            .post('/api/doctor/login')
            .send(doctorWrongPassword)
            .expect(401);
      });

      it('returns expected response when password is wrong', async ()=>{
        const response = await request
            .post('/api/doctor/login')
            .send(doctorWrongPassword);
        expect(response.body.code).to.equal(401);
        expect(response.body.message).to.equal(`Wrong username or password.`);
      });
    });
  });

  describe('Doctors list', ()=>{
    it('returns 200 when doctors are retrieved', async ()=>{
      await request
          .get('/api/doctors')
          .expect(200);
    });

    it('returns expected number of doctors', async ()=>{
      const doctorsResponse = await request
          .get('/api/doctors')
          .expect(200);
      expect(doctorsResponse.body.doctors.length).to.equal(DOCTORS_DB_SIZE);
    });

    it('returns expected values on doctors', async ()=>{
      const doctorsResponse = await request
          .get('/api/doctors')
          .expect(200);
      const doctors = doctorsResponse.body.doctors;
      doctors.forEach((doctor) => {
        expect(doctor.doctor_id).to.be.ok;
        expect(doctor.nombre).to.be.ok;
        expect(doctor.matricula).to.be.ok;
        expect(doctor.especialidades).to.be.ok;
        expect(doctor.horario_atencion).to.be.ok;
        expect(doctor.disponible).to.be.ok;
      });
    });
  });

  describe('Member Register Device', ()=>{
    const validMember = {
      username: '101010',
      password: '101010',
    };

    it('returns 200 when device is registered', async ()=>{
      const memberTokenResponse = await request
        .post('/api/member/login')
        .send(validMember);

      const memberToken = memberTokenResponse.body.token.token;
      const memberAuthHeader = `Bearer ${memberToken}`;
      const memberDeviceToken = {
        device_token: uuidv4(),
      }

      await request
        .post('/api/member/register_device')
        .set('Authorization', memberAuthHeader)
        .send(memberDeviceToken)
        .expect(200);
    });

    it('returns expected device token', async ()=>{
      const memberTokenResponse = await request
        .post('/api/member/login')
        .send(validMember);

      const memberToken = memberTokenResponse.body.token.token;
      const memberAuthHeader = `Bearer ${memberToken}`;
      const memberDeviceToken = {
        device_token: uuidv4(),
      }

      const memberDeviceTokenResponse = await request
        .post('/api/member/register_device')
        .set('Authorization', memberAuthHeader)
        .send(memberDeviceToken)
        .expect(200);
      expect(memberDeviceTokenResponse.body.member_device_token.device_token).to.equal(memberDeviceToken.device_token);
    });
  });

  describe('Member Get Specialties', ()=>{
    const validMember = {
      username: '1111',
      password: '37243062',
    };

    it('returns 200 when specialties are retrieved', async ()=>{
      const memberTokenResponse = await request
          .post('/api/member/login')
          .send(validMember);

      const memberToken = memberTokenResponse.body.token.token;
      const memberAuthHeader = `Bearer ${memberToken}`;

      await request
          .get('/api/member/medical_specialties')
          .set('Authorization', memberAuthHeader)
          .expect(200);
    });

    it('returns expected number of specialties', async ()=>{
      const memberTokenResponse = await request
          .post('/api/member/login')
          .send(validMember);

      const memberToken = memberTokenResponse.body.token.token;
      const memberAuthHeader = `Bearer ${memberToken}`;

      const medicalSpecialtiesResponse = await request
          .get('/api/member/medical_specialties')
          .set('Authorization', memberAuthHeader)
          .expect(200);
      expect(medicalSpecialtiesResponse.body.medical_specialties.length).to.equal(MEDICAL_SPECIALTIES_SIZE);
    });

    it('returns expected values on specialties', async ()=>{
      const memberTokenResponse = await request
          .post('/api/member/login')
          .send(validMember);

      const memberToken = memberTokenResponse.body.token.token;
      const memberAuthHeader = `Bearer ${memberToken}`;

      const medicalSpecialtiesResponse = await request
          .get('/api/member/medical_specialties')
          .set('Authorization', memberAuthHeader)
          .expect(200);
      const medicalSpecialties = medicalSpecialtiesResponse.body.medical_specialties;
      medicalSpecialties.forEach((medicalSpecialty) => {
        expect(medicalSpecialty.medical_specialty_id).to.be.ok;
        expect(medicalSpecialty.name).to.be.ok;
      });
    });
  });

  describe('Member Request Appointment', () =>{
    const validMember = {
      username: '1111',
      password: '37243062',
    };

    const validAppointment = {
      medical_specialty_id: 5,
      reason: 'Fiebre alta',
    };

    it('returns expected values on appointment', async ()=>{
      const memberTokenResponse = await request
          .post('/api/member/login')
          .send(validMember);

      const memberToken = memberTokenResponse.body.token.token;
      const memberAuthHeader = `Bearer ${memberToken}`;

      const currentTime = moment(getCurrentLocalTime());

      const newAppointmentResponse = await request
          .post('/api/member/appointment')
          .set('Authorization', memberAuthHeader)
          .send(validAppointment)
          .expect(200);
      const appointment = newAppointmentResponse.body.appointment;
      expect(appointment.id).to.be.ok;
      expect(appointment.especialidad).to.be.ok;
      expect(appointment.especialidad.medical_specialty_id).to.equal(validAppointment.medical_specialty_id);
      expect(appointment.especialidad.name).to.be.ok;
      expect(appointment.member_id).to.be.ok;
      expect(appointment.doctor_id).to.not.be.ok;
      expect(appointment.estado).to.equal('En cola');
      expect(appointment.channel_id).to.be.ok;
      expect(appointment.motivo).to.equal(validAppointment.reason);
      expect(appointment.personas_en_espera).to.be.ok;
      const appointmentTime = moment(appointment.hora_solicitud);
      expect(appointmentTime.isSameOrAfter(currentTime)).to.be.true;
    });
  });

  describe('Member Get Appointment Status', () =>{
    const validMember = {
      username: '1111',
      password: '37243062',
    };

    const validAppointment = {
      medical_specialty_id: 5,
      reason: 'Fiebre alta',
    };

    it('returns expected values on appointment status', async ()=>{
      const memberTokenResponse = await request
          .post('/api/member/login')
          .send(validMember);

      const memberToken = memberTokenResponse.body.token.token;
      const memberAuthHeader = `Bearer ${memberToken}`;

      const newAppointmentResponse = await request
          .post('/api/member/appointment')
          .set('Authorization', memberAuthHeader)
          .send(validAppointment)
          .expect(200);
      const appointment = newAppointmentResponse.body.appointment;

      const appointmentStatusResponse = await request
          .get(`/api/member/appointment/${appointment.id}`)
          .set('Authorization', memberAuthHeader)
          .send(validAppointment)
          .expect(200);

      const appointmentStatus = appointmentStatusResponse.body.appointment;
      expect(appointmentStatus.id).to.equal(appointment.id);
      expect(appointmentStatus.especialidad.medical_specialty_id).to.equal(appointment.especialidad.medical_specialty_id);
      expect(appointmentStatus.especialidad.name).to.equal(appointment.especialidad.name);
      expect(appointmentStatus.member_id).to.equal(appointment.member_id);
      expect(appointmentStatus.estado).to.equal(appointment.estado);
      expect(appointmentStatus.channel_id).to.equal(appointment.channel_id);
      expect(appointmentStatus.motivo).to.equal(appointment.motivo);
      expect(appointmentStatus.hora_solicitud).to.equal(appointment.hora_solicitud);
    });
  });

  describe('Member Get Appointments', () => {
    const validMember = {
      username: '1111',
      password: '37243062',
    };

    it('returns 200 when appointments are retrieved', async ()=>{
      const memberTokenResponse = await request
          .post('/api/member/login')
          .send(validMember);

      const memberToken = memberTokenResponse.body.token.token;
      const memberAuthHeader = `Bearer ${memberToken}`;

      await request
          .get('/api/member/appointments')
          .set('Authorization', memberAuthHeader)
          .expect(200);
    });

    it('returns expected values on appointments', async ()=>{
      const memberTokenResponse = await request
          .post('/api/member/login')
          .send(validMember);

      const memberToken = memberTokenResponse.body.token.token;
      const memberAuthHeader = `Bearer ${memberToken}`;

      const appointmentsResponse = await request
          .get('/api/member/appointments')
          .set('Authorization', memberAuthHeader)
          .expect(200);

      const appointments = appointmentsResponse.body.appointments;
      appointments.forEach((appointment) => {
        expect(appointment.id).to.be.ok;
        expect(appointment.especialidad).to.be.ok;
        expect(appointment.member_id).to.be.ok;
        expect(appointment.doctor).to.be.ok;
        expect(appointment.doctor.doctor_id).to.be.ok;
        expect(appointment.doctor.nombre).to.be.ok;
        expect(appointment.estado).to.equal('Terminado');
        expect(appointment.motivo).to.be.ok;
        expect(appointment.hora_solicitud).to.be.ok;
      });
    });
  });

  describe('Rate Appointment', () =>{
    const validMember = {
      username: '1111',
      password: '37243062',
    };

    const validAppointment = {
      medical_specialty_id: '5',
      reason: 'Fiebre alta',
    };

    it('returns expected values on appointment rate', async ()=>{
      const memberTokenResponse = await request
          .post('/api/member/login')
          .send(validMember);

      const memberToken = memberTokenResponse.body.token.token;
      const memberAuthHeader = `Bearer ${memberToken}`;

      const newAppointmentResponse = await request
          .post('/api/member/appointment')
          .set('Authorization', memberAuthHeader)
          .send(validAppointment)
          .expect(200);
      const appointment = newAppointmentResponse.body.appointment;
      expect(appointment.id).to.be.ok;

      const appointmentId = appointment.id;
      const mockRating = {
        rating: 4,
        comments: 'Excelente profesional.',
      };
      const appointmentRatingResponse = await request
          .post(`/api/member/appointment/${appointmentId}/rating`)
          .set('Authorization', memberAuthHeader)
          .send(mockRating)
          .expect(200);

      const appointmentRating = appointmentRatingResponse.body.appointment_rating;
      expect(appointmentRating.appointment_id).to.equal(appointmentId);
      expect(appointmentRating.rating).to.equal(mockRating.rating);
      expect(appointmentRating.comments).to.equal(mockRating.comments);
    });
  });

  describe('Doctor Get Appointments', () => {
    const validDoctor = {
      username: '1111',
      password: '1111',
    };

    it('returns 200 when appointments are retrieved', async ()=>{
      const doctorTokenResponse = await request
          .post('/api/doctor/login')
          .send(validDoctor);

      const doctorToken = doctorTokenResponse.body.token.token;
      const doctorAuthHeader = `Bearer ${doctorToken}`;

      await request
          .get('/api/doctor/appointments')
          .set('Authorization', doctorAuthHeader)
          .expect(200);
    });

    it('returns expected values on appointments', async ()=>{
      const doctorTokenResponse = await request
          .post('/api/doctor/login')
          .send(validDoctor);

      const doctorToken = doctorTokenResponse.body.token.token;
      const doctorAuthHeader = `Bearer ${doctorToken}`;

      const appointmentsResponse = await request
          .get('/api/doctor/appointments')
          .set('Authorization', doctorAuthHeader)
          .expect(200);
      const appointments = appointmentsResponse.body.appointments;
      appointments.forEach((appointment) => {
        expect(appointment.id).to.be.ok;
        expect(appointment.especialidad).to.be.ok;
        expect(appointment.doctor_id).to.be.ok;
        expect(appointment.member).to.be.ok;
        expect(appointment.member.member_id).to.be.ok;
        expect(appointment.member.nombre).to.be.ok;
        expect(appointment.estado).to.equal('Terminado');
        expect(appointment.motivo).to.be.ok;
        expect(appointment.hora_solicitud).to.be.ok;
      });
    });
  });

  describe('Doctor Queued Appointments', () => {
    const validDoctor = {
      username: '1111',
      password: '1111',
    };

    it('returns 200 when queued appointments are retrieved', async ()=>{
      const doctorTokenResponse = await request
          .post('/api/doctor/login')
          .send(validDoctor);

      const doctorToken = doctorTokenResponse.body.token.token;
      const doctorAuthHeader = `Bearer ${doctorToken}`;

      await request
          .get('/api/doctor/queued_appointments')
          .set('Authorization', doctorAuthHeader)
          .expect(200);
    });

    it('returns expected values on queued appointments', async ()=>{
      const doctorTokenResponse = await request
          .post('/api/doctor/login')
          .send(validDoctor);

      const doctorToken = doctorTokenResponse.body.token.token;
      const doctorAuthHeader = `Bearer ${doctorToken}`;

      const appointmentsResponse = await request
          .get('/api/doctor/queued_appointments')
          .set('Authorization', doctorAuthHeader)
          .expect(200);
      const appointments = appointmentsResponse.body.queued_appointments;
      appointments.forEach((appointment) => {
        expect(appointment.id).to.be.ok;
        expect(appointment.especialidad).to.be.ok;
        expect(appointment.member).to.be.ok;
        expect(appointment.member.member_id).to.be.ok;
        expect(appointment.member.nombre).to.be.ok;
        expect(appointment.estado).to.equal('En cola');
        expect(appointment.qr_conexion).to.be.ok;
        expect(appointment.motivo).to.be.ok;
        expect(appointment.hora_solicitud).to.be.ok;
        expect(appointment.tiempo_espera).to.be.ok;
      });
    });
  });

  describe('Virtual appointments', ()=>{
    const validMember = {
      username: '1111',
      password: '37243062',
    };

    const validDoctor = {
      username: '1111',
      password: '1111',
    };

    const validAppointment = {
      medical_specialty_id: '5',
      reason: 'Fiebre alta',
    };

    describe('Happy path', ()=>{
      it('new appointment, doctor connects, member connects, member disconnects, doctor disconnects', async ()=>{
        const memberTokenResponse = await request
            .post('/api/member/login')
            .send(validMember);

        const memberToken = memberTokenResponse.body.token.token;
        const memberAuthHeader = `Bearer ${memberToken}`;

        const newAppointmentResponse = await request
            .post('/api/member/appointment')
            .set('Authorization', memberAuthHeader)
            .send(validAppointment)
            .expect(200);
        const appointment = newAppointmentResponse.body.appointment;
        expect(appointment.id).to.be.ok;
        expect(appointment.estado).to.equal('En cola');
        expect(appointment.channel_id).to.be.ok;

        const appointmentId = appointment.id;

        const doctorTokenResponse = await request
            .post('/api/doctor/login')
            .send(validDoctor);

        const doctorToken = doctorTokenResponse.body.token.token;
        const doctorAuthHeader = `Bearer ${doctorToken}`;

        const appointmentDoctorConnectResponse = await request
            .post(`/api/doctor/appointment/${appointmentId}/start`)
            .set('Authorization', doctorAuthHeader)
            .expect(200);

        const appointmentDoctorConnect = appointmentDoctorConnectResponse.body.appointment;
        expect(appointmentDoctorConnect.id).to.equal(appointmentId);
        expect(appointmentDoctorConnect.estado).to.equal('Esperando afiliado');

        const appointmentMemberConnectResponse = await request
            .post(`/api/member/appointment/${appointmentId}/start`)
            .set('Authorization', memberAuthHeader)
            .expect(200);

        const appointmentMemberConnect = appointmentMemberConnectResponse.body.appointment;
        expect(appointmentMemberConnect.id).to.equal(appointmentId);
        expect(appointmentMemberConnect.estado).to.equal('En curso');

        const appointmentMemberDisconnectResponse = await request
            .post(`/api/member/appointment/${appointmentId}/finish`)
            .set('Authorization', memberAuthHeader)
            .expect(200);

        const appointmentMemberDisconnect = appointmentMemberDisconnectResponse.body.appointment;
        expect(appointmentMemberDisconnect.id).to.equal(appointmentId);
        expect(appointmentMemberDisconnect.estado).to.equal('En curso');

        const appointmentDoctorDisconnectResponse = await request
            .post(`/api/doctor/appointment/${appointmentId}/finish`)
            .set('Authorization', doctorAuthHeader)
            .expect(200);

        const appointmentDoctorDisconnect = appointmentDoctorDisconnectResponse.body.appointment;
        expect(appointmentDoctorDisconnect.id).to.equal(appointmentId);
        expect(appointmentDoctorDisconnect.estado).to.equal('Terminado');
      });

      it('new appointment, doctor connects, member connects, doctor disconnects, member disconnects', async ()=>{
        const memberTokenResponse = await request
            .post('/api/member/login')
            .send(validMember);

        const memberToken = memberTokenResponse.body.token.token;
        const memberAuthHeader = `Bearer ${memberToken}`;

        const newAppointmentResponse = await request
            .post('/api/member/appointment')
            .set('Authorization', memberAuthHeader)
            .send(validAppointment)
            .expect(200);
        const appointment = newAppointmentResponse.body.appointment;
        expect(appointment.id).to.be.ok;
        expect(appointment.estado).to.equal('En cola');
        expect(appointment.channel_id).to.be.ok;

        const appointmentId = appointment.id;

        const doctorTokenResponse = await request
            .post('/api/doctor/login')
            .send(validDoctor);

        const doctorToken = doctorTokenResponse.body.token.token;
        const doctorAuthHeader = `Bearer ${doctorToken}`;

        const appointmentDoctorConnectResponse = await request
            .post(`/api/doctor/appointment/${appointmentId}/start`)
            .set('Authorization', doctorAuthHeader)
            .expect(200);

        const appointmentDoctorConnect = appointmentDoctorConnectResponse.body.appointment;
        expect(appointmentDoctorConnect.id).to.equal(appointmentId);
        expect(appointmentDoctorConnect.estado).to.equal('Esperando afiliado');

        const appointmentMemberConnectResponse = await request
            .post(`/api/member/appointment/${appointmentId}/start`)
            .set('Authorization', memberAuthHeader)
            .expect(200);

        const appointmentMemberConnect = appointmentMemberConnectResponse.body.appointment;
        expect(appointmentMemberConnect.id).to.equal(appointmentId);
        expect(appointmentMemberConnect.estado).to.equal('En curso');

        const appointmentDoctorDisconnectResponse = await request
            .post(`/api/doctor/appointment/${appointmentId}/finish`)
            .set('Authorization', doctorAuthHeader)
            .expect(200);

        const appointmentDoctorDisconnect = appointmentDoctorDisconnectResponse.body.appointment;
        expect(appointmentDoctorDisconnect.id).to.equal(appointmentId);
        expect(appointmentDoctorDisconnect.estado).to.equal('En curso');

        const appointmentMemberDisconnectResponse = await request
            .post(`/api/member/appointment/${appointmentId}/finish`)
            .set('Authorization', memberAuthHeader)
            .expect(200);

        const appointmentMemberDisconnect = appointmentMemberDisconnectResponse.body.appointment;
        expect(appointmentMemberDisconnect.id).to.equal(appointmentId);
        expect(appointmentMemberDisconnect.estado).to.equal('Terminado');
      });
    });

    describe('Errors', () => {
      it('new appointment, member connects, returns error', async ()=>{
        const memberTokenResponse = await request
            .post('/api/member/login')
            .send(validMember);

        const memberToken = memberTokenResponse.body.token.token;
        const memberAuthHeader = `Bearer ${memberToken}`;

        const newAppointmentResponse = await request
            .post('/api/member/appointment')
            .set('Authorization', memberAuthHeader)
            .send(validAppointment)
            .expect(200);
        const appointment = newAppointmentResponse.body.appointment;
        expect(appointment.id).to.be.ok;
        expect(appointment.estado).to.equal('En cola');
        expect(appointment.channel_id).to.be.ok;

        const appointmentId = appointment.id;
        const appointmentMemberConnectResponse = await request
            .post(`/api/member/appointment/${appointmentId}/start`)
            .set('Authorization', memberAuthHeader)
            .expect(500);

        const appointmentMemberConnect = appointmentMemberConnectResponse.body;
        expect(appointmentMemberConnect.code).to.equal(500);
        expect(appointmentMemberConnect.message).to.equal('Invalid appointment action');
      });

      it('new appointment, doctor connects twice, returns error', async ()=>{
        const memberTokenResponse = await request
            .post('/api/member/login')
            .send(validMember);

        const memberToken = memberTokenResponse.body.token.token;
        const memberAuthHeader = `Bearer ${memberToken}`;

        const newAppointmentResponse = await request
            .post('/api/member/appointment')
            .set('Authorization', memberAuthHeader)
            .send(validAppointment)
            .expect(200);
        const appointment = newAppointmentResponse.body.appointment;
        expect(appointment.id).to.be.ok;
        expect(appointment.estado).to.equal('En cola');
        expect(appointment.channel_id).to.be.ok;

        const doctorTokenResponse = await request
            .post('/api/doctor/login')
            .send(validDoctor);

        const doctorToken = doctorTokenResponse.body.token.token;
        const doctorAuthHeader = `Bearer ${doctorToken}`;

        const appointmentId = appointment.id;
        const appointmentDoctorConnectResponse = await request
            .post(`/api/doctor/appointment/${appointmentId}/start`)
            .set('Authorization', doctorAuthHeader)
            .expect(200);

        const appointmentDoctorConnect = appointmentDoctorConnectResponse.body.appointment;
        expect(appointmentDoctorConnect.id).to.equal(appointmentId);
        expect(appointmentDoctorConnect.estado).to.equal('Esperando afiliado');

        const appointmentDoctorConnectResponse2 = await request
            .post(`/api/doctor/appointment/${appointmentId}/start`)
            .set('Authorization', doctorAuthHeader)
            .expect(500);

        const appointmentDoctorConnect2 = appointmentDoctorConnectResponse2.body;
        expect(appointmentDoctorConnect2.code).to.equal(500);
        expect(appointmentDoctorConnect2.message).to.equal('Not able to assign appointment to doctor');
      });

      it('new appointment, doctor connects, member connects, member disconnects twice, returns error', async ()=>{
        const memberTokenResponse = await request
            .post('/api/member/login')
            .send(validMember);

        const memberToken = memberTokenResponse.body.token.token;
        const memberAuthHeader = `Bearer ${memberToken}`;

        const newAppointmentResponse = await request
            .post('/api/member/appointment')
            .set('Authorization', memberAuthHeader)
            .send(validAppointment)
            .expect(200);
        const appointment = newAppointmentResponse.body.appointment;
        expect(appointment.id).to.be.ok;
        expect(appointment.estado).to.equal('En cola');
        expect(appointment.channel_id).to.be.ok;

        const doctorTokenResponse = await request
            .post('/api/doctor/login')
            .send(validDoctor);

        const doctorToken = doctorTokenResponse.body.token.token;
        const doctorAuthHeader = `Bearer ${doctorToken}`;

        const appointmentId = appointment.id;
        const appointmentDoctorConnectResponse = await request
            .post(`/api/doctor/appointment/${appointmentId}/start`)
            .set('Authorization', doctorAuthHeader)
            .expect(200);

        const appointmentDoctorConnect = appointmentDoctorConnectResponse.body.appointment;
        expect(appointmentDoctorConnect.id).to.equal(appointmentId);
        expect(appointmentDoctorConnect.estado).to.equal('Esperando afiliado');

        const appointmentMemberConnectResponse = await request
            .post(`/api/member/appointment/${appointmentId}/start`)
            .set('Authorization', memberAuthHeader)
            .expect(200);

        const appointmentMemberConnect = appointmentMemberConnectResponse.body.appointment;
        expect(appointmentMemberConnect.id).to.equal(appointmentId);
        expect(appointmentMemberConnect.estado).to.equal('En curso');


        const appointmentMemberDisconnectResponse = await request
            .post(`/api/member/appointment/${appointmentId}/finish`)
            .set('Authorization', memberAuthHeader)
            .expect(200);

        const appointmentMemberDisconnect = appointmentMemberDisconnectResponse.body.appointment;
        expect(appointmentMemberDisconnect.id).to.equal(appointmentId);
        expect(appointmentMemberDisconnect.estado).to.equal('En curso');

        const appointmentMemberDisconnectResponse2 = await request
            .post(`/api/member/appointment/${appointmentId}/finish`)
            .set('Authorization', memberAuthHeader)
            .expect(500);

        const appointmentMemberDisconnect2 = appointmentMemberDisconnectResponse2.body;
        expect(appointmentMemberDisconnect2.code).to.equal(500);
        expect(appointmentMemberDisconnect2.message).to.equal('Invalid appointment action');
      });

      it('new appointment, doctor connects, member connects, doctor disconnects twice, returns error', async ()=>{
        const memberTokenResponse = await request
            .post('/api/member/login')
            .send(validMember);

        const memberToken = memberTokenResponse.body.token.token;
        const memberAuthHeader = `Bearer ${memberToken}`;

        const newAppointmentResponse = await request
            .post('/api/member/appointment')
            .set('Authorization', memberAuthHeader)
            .send(validAppointment)
            .expect(200);
        const appointment = newAppointmentResponse.body.appointment;
        expect(appointment.id).to.be.ok;
        expect(appointment.estado).to.equal('En cola');
        expect(appointment.channel_id).to.be.ok;

        const doctorTokenResponse = await request
            .post('/api/doctor/login')
            .send(validDoctor);

        const doctorToken = doctorTokenResponse.body.token.token;
        const doctorAuthHeader = `Bearer ${doctorToken}`;

        const appointmentId = appointment.id;
        const appointmentDoctorConnectResponse = await request
            .post(`/api/doctor/appointment/${appointmentId}/start`)
            .set('Authorization', doctorAuthHeader)
            .expect(200);

        const appointmentDoctorConnect = appointmentDoctorConnectResponse.body.appointment;
        expect(appointmentDoctorConnect.id).to.equal(appointmentId);
        expect(appointmentDoctorConnect.estado).to.equal('Esperando afiliado');

        const appointmentMemberConnectResponse = await request
            .post(`/api/member/appointment/${appointmentId}/start`)
            .set('Authorization', memberAuthHeader)
            .expect(200);

        const appointmentMemberConnect = appointmentMemberConnectResponse.body.appointment;
        expect(appointmentMemberConnect.id).to.equal(appointmentId);
        expect(appointmentMemberConnect.estado).to.equal('En curso');

        const appointmentDoctorDisconnectResponse = await request
            .post(`/api/doctor/appointment/${appointmentId}/finish`)
            .set('Authorization', doctorAuthHeader)
            .expect(200);

        const appointmentDoctorDisconnect = appointmentDoctorDisconnectResponse.body.appointment;
        expect(appointmentDoctorDisconnect.id).to.equal(appointmentId);
        expect(appointmentDoctorDisconnect.estado).to.equal('En curso');

        const appointmentDoctorDisconnectResponse2 = await request
            .post(`/api/doctor/appointment/${appointmentId}/finish`)
            .set('Authorization', doctorAuthHeader)
            .expect(500);

        const appointmentDoctorDisconnect2 = appointmentDoctorDisconnectResponse2.body;
        expect(appointmentDoctorDisconnect2.code).to.equal(500);
        expect(appointmentDoctorDisconnect2.message).to.equal('Invalid appointment action');
      });
    });
  });

  describe('Doctor Add Appointment Data', () => {

    const validMember = {
      username: '1111',
      password: '37243062',
    };

    const validDoctor = {
      username: '1111',
      password: '1111',
    };

    const intruderDoctor = {
      username: '2222',
      password: '2222',
    }

    const validAppointment = {
      medical_specialty_id: '5',
      reason: 'Fiebre alta',
    };

    const validAppointmentData = {
      indications: 'Reposo durante 24 horas. Ibuprofeno cada 8 horas.'
    };

    it('valid doctor adds appointment data', async () => {
        const memberTokenResponse = await request
          .post('/api/member/login')
          .send(validMember);

        const memberToken = memberTokenResponse.body.token.token;
        const memberAuthHeader = `Bearer ${memberToken}`;

        const newAppointmentResponse = await request
          .post('/api/member/appointment')
          .set('Authorization', memberAuthHeader)
          .send(validAppointment);

        const appointment = newAppointmentResponse.body.appointment;
        const appointmentId = appointment.id;

        const doctorTokenResponse = await request
          .post('/api/doctor/login')
          .send(validDoctor);

        const doctorToken = doctorTokenResponse.body.token.token;
        const doctorAuthHeader = `Bearer ${doctorToken}`;

        await request.post(`/api/doctor/appointment/${appointmentId}/start`)
          .set('Authorization', doctorAuthHeader);

        await request.post(`/api/member/appointment/${appointmentId}/start`)
          .set('Authorization', memberAuthHeader);

        await request.post(`/api/member/appointment/${appointmentId}/finish`)
          .set('Authorization', memberAuthHeader);

        await request.post(`/api/doctor/appointment/${appointmentId}/finish`)
          .set('Authorization', doctorAuthHeader);

        const appointmentDataResponse = await request
          .post(`/api/doctor/appointment/${appointmentId}/data`)
          .set('Authorization', doctorAuthHeader)
          .send(validAppointmentData)
          .expect(200);

        expect(appointmentDataResponse.body.appointment.id).to.equal(appointmentId);
        expect(appointmentDataResponse.body.appointment.informacion).to.not.be.undefined;
        expect(appointmentDataResponse.body.appointment.informacion.indicaciones).to.equal(validAppointmentData.indications);
      });

    it('intruder doctor adds appointment data', async () => {
      const memberTokenResponse = await request
        .post('/api/member/login')
        .send(validMember);

      const memberToken = memberTokenResponse.body.token.token;
      const memberAuthHeader = `Bearer ${memberToken}`;

      const newAppointmentResponse = await request
        .post('/api/member/appointment')
        .set('Authorization', memberAuthHeader)
        .send(validAppointment);

      const appointment = newAppointmentResponse.body.appointment;
      const appointmentId = appointment.id;

      const doctorTokenResponse = await request
        .post('/api/doctor/login')
        .send(validDoctor);

      const doctorToken = doctorTokenResponse.body.token.token;
      const doctorAuthHeader = `Bearer ${doctorToken}`;

      await request.post(`/api/doctor/appointment/${appointmentId}/start`)
        .set('Authorization', doctorAuthHeader);

      await request.post(`/api/member/appointment/${appointmentId}/start`)
        .set('Authorization', memberAuthHeader);

      await request.post(`/api/member/appointment/${appointmentId}/finish`)
        .set('Authorization', memberAuthHeader);

      await request.post(`/api/doctor/appointment/${appointmentId}/finish`)
        .set('Authorization', doctorAuthHeader);

      const intruderDoctorTokenResponse = await request
        .post('/api/doctor/login')
        .send(intruderDoctor);

      const intruderDoctorToken = intruderDoctorTokenResponse.body.token.token;
      const intruderDoctorAuthHeader = `Bearer ${intruderDoctorToken}`;

      const appointmentDataResponse = await request
        .post(`/api/doctor/appointment/${appointmentId}/data`)
        .set('Authorization', intruderDoctorAuthHeader)
        .send(validAppointmentData)
        .expect(500);

      expect(appointmentDataResponse.body.code).to.equal(500);
      expect(appointmentDataResponse.body.message).to.equal('Another doctor has already answered the appointment');
    });
  });
});
